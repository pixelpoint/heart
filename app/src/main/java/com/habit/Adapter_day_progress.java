package com.habit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.habit.anulomvilom.Anulom_Activity;
import com.habit.bhramari.BhramariPranayama_Activity;
import com.habit.data.ReportModel;
import com.habit.kapalbhati.Kapalbhati_Activity;
import com.habit.report.CatReportActivity;
import com.habit.setubandhasana.Activity_SetuBandhasana;
import com.habit.virbhadrasana.Virabhadrasana_Activity;

import java.util.List;

/**
 * Created by Hitesh on 05-Jan-17.
 */

public class Adapter_day_progress extends RecyclerView.Adapter<Adapter_day_progress.MyViewHolder> {

    private List<ReportModel> reportModelList;
    private String type;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_day;
        ProgressBar pb_step;
        LinearLayout ll_parent;


        public MyViewHolder(View view) {
            super(view);
            tv_day          = (TextView) view.findViewById(R.id.tv_day);
            pb_step         = (ProgressBar) view.findViewById(R.id.progress_step);
            ll_parent       = (LinearLayout) view.findViewById(R.id.ll_parent);

        }
    }

    public Adapter_day_progress(List<ReportModel> reportModelList,Context context) {
        this.reportModelList = reportModelList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_day_and_progress, parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ReportModel reportModel = reportModelList.get(position);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width/7, ViewGroup.LayoutParams.WRAP_CONTENT);

        holder.ll_parent.setLayoutParams(layoutParams);

        holder.tv_day.setText(reportModel.getDate());
        holder.pb_step.setMax(reportModel.getMax_steps());
        holder.pb_step.setProgress(reportModel.getTotal_steps());
        if (position==6)
            holder.tv_day.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
    }

    @Override
    public int getItemCount() {
        return reportModelList.size();
    }

}
