package com.habit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.habit.charting.DemoBase;
import com.habit.charting.charts.BarChart;
import com.habit.charting.components.Legend;
import com.habit.charting.components.XAxis;
import com.habit.charting.components.YAxis;
import com.habit.charting.data.BarData;
import com.habit.charting.data.BarDataSet;
import com.habit.charting.data.BarEntry;
import com.habit.charting.data.Entry;
import com.habit.charting.formatter.IAxisValueFormatter;
import com.habit.charting.highlight.Highlight;
import com.habit.charting.interfaces.datasets.IBarDataSet;
import com.habit.charting.listener.OnChartValueSelectedListener;
import com.habit.charting.utils.ColorTemplate;
import com.habit.charting.utils.MPPointF;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;
import com.habit.homescreen.Home_Screen_Activity;
import com.habit.report.ReportAdapter;
import com.habit.report.ReportAdapter_Yogasana;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class MainReportActivity extends DemoBase implements
        OnChartValueSelectedListener {

    Context context;
    BarChart mChart;
    TextView tv_step;
    ProgressBar pb_steps;
    DatabaseHandler db;
    ReportModel reportModel,report_day_progress;
    ArrayList<ReportModel> pranayama_time;
    ArrayList<ReportModel> total_steps;
    ArrayList<ReportModel> list_vir,list_setu;
    ArrayList<ReportModel> list_day_progress;
    ArrayList<Integer> list_yoga;
    ArrayList<Float> dotime;
    float tt_time=0;
    int tt_step=0,count_step=0;
    ListView[] list_days;
    TextView[] tv_days,tv_date;
    TextView tv_main_date;
    String st_date="",end_date="";
    ReportAdapter_Yogasana adapter;
    int btn_clk=0;
    ImageView iv_start,iv_end,iv_back;
    SimpleDateFormat dateFormatsep = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    RecyclerView rv_pr_steps;
    Adapter_day_progress adapter_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_report);

        context         = this;
        mChart          = (BarChart) findViewById(R.id.chart1);
        rv_pr_steps     = (RecyclerView) findViewById(R.id.rv_progress_step);

        tv_step         = (TextView) findViewById(R.id.tv_steps);

        pb_steps        = (ProgressBar) findViewById(R.id.progress_step);
        tv_main_date    = (TextView) findViewById(R.id.tv_date);
        iv_start        = (ImageView) findViewById(R.id.iv_start);
        iv_end          = (ImageView) findViewById(R.id.iv_end);
        iv_back         = (ImageView) findViewById(R.id.im_backbutton);

        tv_days         = new TextView[7];
        tv_days[0]      = (TextView)findViewById(R.id.tv_mon);
        tv_days[1]      = (TextView)findViewById(R.id.tv_tue);
        tv_days[2]      = (TextView)findViewById(R.id.tv_wed);
        tv_days[3]      = (TextView)findViewById(R.id.tv_thu);
        tv_days[4]      = (TextView)findViewById(R.id.tv_fri);
        tv_days[5]      = (TextView)findViewById(R.id.tv_sat);
        tv_days[6]      = (TextView)findViewById(R.id.tv_sun);

        tv_date         = new TextView[7];
        tv_date[0]      = (TextView)findViewById(R.id.tv_date1);
        tv_date[1]      = (TextView)findViewById(R.id.tv_date2);
        tv_date[2]      = (TextView)findViewById(R.id.tv_date3);
        tv_date[3]      = (TextView)findViewById(R.id.tv_date4);
        tv_date[4]      = (TextView)findViewById(R.id.tv_date5);
        tv_date[5]      = (TextView)findViewById(R.id.tv_date6);
        tv_date[6]      = (TextView)findViewById(R.id.tv_date7);

        list_days       = new ListView[7];
        list_days[0]    = (ListView) findViewById(R.id.list_mon);
        list_days[1]    = (ListView) findViewById(R.id.list_tue);
        list_days[2]    = (ListView) findViewById(R.id.list_wed);
        list_days[3]    = (ListView) findViewById(R.id.list_thu);
        list_days[4]    = (ListView) findViewById(R.id.list_fri);
        list_days[5]    = (ListView) findViewById(R.id.list_sat);
        list_days[6]    = (ListView) findViewById(R.id.list_sun);

        reportModel     = new ReportModel();
        pranayama_time  = new ArrayList<ReportModel>();
        total_steps     = new ArrayList<ReportModel>();
        list_vir        = new ArrayList<ReportModel>();
        list_setu       = new ArrayList<ReportModel>();
        list_day_progress=new ArrayList<ReportModel>();
        list_yoga       = new ArrayList<Integer>();
        dotime          = new ArrayList<Float>();
        db              = new DatabaseHandler(context);

        //setup chart
        setChartData();

        setDefaultData(dateFormatsep.format(new Date()));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (get_count_of_days(GlobalClass.callSavedPreferences("installdate", context), st_date)) {
                Date date = null;
                try {
                    date = dateFormatsep.parse(st_date);
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE, -1);

                    Date resultdate = new Date(c.getTimeInMillis());
                    String temp_date = dateFormatsep.format(resultdate);

                    setDefaultData(temp_date);

                    btn_clk = btn_clk + 1;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            }
        });

            iv_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btn_clk>0){
                    Date date                      = null;
                    try {
                        date = dateFormatsep.parse(end_date);

                    Calendar c                     = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE,7);

                    Date resultdate                = new Date(c.getTimeInMillis());
                    String temp_date               = dateFormatsep.format(resultdate);

                    setDefaultData(temp_date);
                    btn_clk-=1;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public void setChartData(){

        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart,context);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        mChart.getLegend().setEnabled(false);

        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
    }

    @Override
    protected float getRandom(float range, float startsfrom) {
        return super.getRandom(range, startsfrom);
    }


    private void setData(ArrayList<Float> list) {
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < list.size(); i++) {
            //float mult = (range + 1);
           // float val = (float) (dotime.get(i%dotime.size()));
            yVals1.add(new BarEntry(i, list.get(i%list.size())));
        }

        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "The year 2017");
            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);

            mChart.setData(data);
        }
    }

    protected RectF mOnValueSelectedRectF = new RectF();

    @SuppressLint("NewApi")
    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        mChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = mChart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + mChart.getLowestVisibleX() + ", high: "
                        + mChart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }

    public String day_of_week(int day_of_week){
        String day_week="SUN";
        switch (day_of_week) {
            case Calendar.SUNDAY:
                day_week = "SUN";
                break;

            case Calendar.MONDAY:
                day_week = "MON";
                break;

            case Calendar.TUESDAY:
                day_week = "TUE";
                break;
            case Calendar.WEDNESDAY:
                day_week = "WED";
                break;
            case Calendar.THURSDAY:
                day_week = "THU";
                break;
            case Calendar.FRIDAY:
                day_week = "FRI";
                break;
            case Calendar.SATURDAY:
                day_week = "SAT";
                break;
            default:
                day_week = "SUN";
                break;

        }
        return day_week;
    }

    public String monthYear(int month_of) {
        String month_yr="JAN";
        switch (month_of) {
            case Calendar.JANUARY:
                month_yr="JAN";
                break;

            case Calendar.FEBRUARY:
                month_yr="FEB";
                break;

            case Calendar.MARCH:
                month_yr="MAR";
                break;
            case Calendar.APRIL:
                month_yr="APR";
                break;

            case Calendar.MAY:
                month_yr="MAY";
                break;

            case Calendar.JUNE:
                month_yr="JUN";
                break;

            case Calendar.JULY:
                month_yr="JUL";
                break;
            case Calendar.AUGUST:
                month_yr="AUG";
                break;
            case Calendar.SEPTEMBER:
                month_yr="SEP";
                break;
            case Calendar.OCTOBER:
                month_yr="OCT";
                break;
            case Calendar.NOVEMBER:
                month_yr="NOV";
                break;
            case Calendar.DECEMBER:
                month_yr="DEC";
                break;
            default:
                month_yr="JAN";
                break;
        }
        return  month_yr;
    }

    public void setDefaultData(String curnt_dt){
        try {
            //String current_date            = dateFormatsep.format(new Date());
            Date date                      = dateFormatsep.parse(curnt_dt);
            Calendar c                     = Calendar.getInstance();
            c.setTime(date);
            end_date                       = curnt_dt;
            c.add(Calendar.DATE,-7);
            tt_step    = 0;
            count_step = 0;
            list_day_progress = new ArrayList<ReportModel>();
//Get data of one week
            for (int i=0;i<=6;i++){
                report_day_progress = new ReportModel();
                c.add(Calendar.DATE,1);
                tv_days[i].setText(day_of_week(c.get(Calendar.DAY_OF_WEEK)));
                tv_date[i].setText(c.get(Calendar.DATE)+"");

                report_day_progress.setDate(day_of_week(c.get(Calendar.DAY_OF_WEEK)));

                Date resultdate                = new Date(c.getTimeInMillis());
                String temp_date               = dateFormatsep.format(resultdate);
                if (i==0){
                    tv_main_date.setText(monthYear(c.get(Calendar.MONTH))+" "+c.get(Calendar.DATE));
                    st_date = temp_date;
                }
                if (i==6){
                    tv_main_date.setText(tv_main_date.getText().toString()+" - "+monthYear(c.get(Calendar.MONTH))+" "+c.get(Calendar.DATE));
                    end_date = temp_date;
                }
//Get steps of one week
                total_steps = db.getSteps(temp_date);

                if (!total_steps.isEmpty()){
                    reportModel = total_steps.get(0);
                    tt_step     = tt_step+reportModel.getMax_steps();
                    count_step  = count_step+reportModel.getTotal_steps();
                    report_day_progress.setMax_steps(reportModel.getMax_steps());
                    report_day_progress.setTotal_steps(reportModel.getTotal_steps());
                }
                else {
                    report_day_progress.setMax_steps(1000);
                    report_day_progress.setTotal_steps(0);
                }
//Get Yogasana data
                list_yoga = new ArrayList<Integer>();
                list_vir  = new ArrayList<ReportModel>();
                list_setu = new ArrayList<ReportModel>();

                list_vir  = db.getYoga(1,temp_date);

                if (!list_vir.isEmpty()){
                    reportModel = new ReportModel();
                    reportModel = list_vir.get(0);
                    list_yoga.add(reportModel.getStts_yoga());
                }
                else
                    list_yoga.add(0);

                list_setu = db.getYoga(2,temp_date);

                if (!list_setu.isEmpty()){
                    reportModel = new ReportModel();
                    reportModel = list_setu.get(0);
                    list_yoga.add(reportModel.getStts_yoga());
                }
                else
                    list_yoga.add(0);

                adapter  = new ReportAdapter_Yogasana(context,R.layout.adapter_yogasana_report,list_yoga);
                list_days[i].setAdapter(adapter);
                list_days[i].setEnabled(false);

                list_day_progress.add(report_day_progress);
            }

            adapter_progress = new Adapter_day_progress(list_day_progress,context);
            rv_pr_steps.setAdapter(adapter_progress);
//set steps
            tv_step.setText(count_step+"/"+tt_step+"\n"+getResources().getString(R.string.Step));

            pb_steps.setMax(tt_step);
            pb_steps.setProgress(count_step);

//Get pranayama time of one week
            dotime = new ArrayList<Float>();
            for (int k=1;k<=3;k++){
                tt_time = 0;
                pranayama_time = new ArrayList<ReportModel>();
                reportModel    = new ReportModel();
                pranayama_time = db.getPranayamaTimes(k,st_date,end_date);

                if (!pranayama_time.isEmpty()){
                    reportModel = pranayama_time.get(0);
                    tt_time     = reportModel.getDo_time();
                }
                dotime.add(tt_time);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        setData(dotime);
        mChart.invalidate();
    }

    public boolean get_count_of_days(String created_date_String, String Expire_date_String) {
        boolean result=false;
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = formatter.parse(created_date_String);

            Date date2 = formatter.parse(Expire_date_String);

            if (date1.compareTo(date2)<0)
            {
                System.out.println("date2 is Greater than my date1");
                result = true;
            }
            else
                result = false;

        }catch (ParseException e1){
            e1.printStackTrace();
        }

        return result;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(context, Home_Screen_Activity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
