package com.habit;

import com.habit.charting.components.AxisBase;
import com.habit.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public MyAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return getTime((int) value);
    }

    public String getTime(int duration) {

        int hr = duration / 3600;
        int v = duration % 3600;
        int min = v / 60;
        int sec = v % 60;

        String time="";
        if(hr!=0){

            if(min!=0)
            {
                if(sec!=0)
                    time =""+hr+"hour"+" "+min+"min"+" "+sec+"sec";
                else
                    time =""+hr+"hour"+" "+min+"min";
            }
            else{
                if(sec!=0)
                    time =""+hr+"hour"+" "+sec+"sec";
                else
                    time =""+hr+"hour";
            }
        }
        else{
            if(min!=0)
            {
                if(sec!=0)
                    time = ""+min+"min"+" "+sec+"sec";
                else
                    time = ""+min+"min";
            }
            else{
                time = ""+sec+"sec";
            }
        }

        return time;
    }
}
