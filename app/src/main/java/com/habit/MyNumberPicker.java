package com.habit;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by india on 11-May-16.
 */
public class MyNumberPicker extends NumberPicker{
    public MyNumberPicker(Context context) {
        super(context);
    }

    public MyNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void addView(View child){
        super.addView(child);
        updateView(child);

    }
    public void addView(View child,int index,ViewGroup.LayoutParams params){
        super.addView(child,index,params);
        updateView(child);
    }
    public void addView(View child,ViewGroup.LayoutParams params){
        super.addView(child,params);
        updateView(child);
    }

    private void updateView(View view) {
        if (view instanceof EditText){
            ((EditText)view).setTextSize(20);
            ((EditText)view).setTextColor(getResources().getColor(R.color.Text));

        }
    }

}
