package com.habit;

/**
 * Created by Hitesh on 31-Mar-16.
 */
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.habit.data.GlobalClass;
import com.habit.stepcounter.Step_Counter_Activity;

public class NotificationReceiver extends BroadcastReceiver {
    int MID=0;
    Intent notificationIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        GlobalClass.savePreferences2("habit_start",1,context);
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationIntent = new Intent(context, Step_Counter_Activity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.foot_icon)
                .setContentTitle("7Pranayama")
                .setContentText("Hi, Time to do Pranayam!")
                .setSound(alarmSound)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.colorAccent));
        mNotifyBuilder.setWhen(when);
        mNotifyBuilder.setContentIntent(pendingIntent);
        mNotifyBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        notificationManager.notify(MID, mNotifyBuilder.build());
        MID++;

    } else {
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.foot_icon)
                .setContentTitle("7Pranayama")
                .setContentText("Hi, Time to do Pranayam!")
                .setSound(alarmSound)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.colorAccent));
        mNotifyBuilder.setWhen(when);
        mNotifyBuilder.setContentIntent(pendingIntent);
        mNotifyBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        notificationManager.notify(MID, mNotifyBuilder.build());
        MID++;
    }

    }

}