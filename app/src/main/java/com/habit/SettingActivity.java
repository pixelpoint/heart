package com.habit;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.habit.data.GlobalClass;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class SettingActivity extends Activity {
    Context context;
    ToggleButton sound,sec_sound,display,vib,noti;
    TextView tv_sound,tv_sound_sec,tv_vibration,tv_display,tv_volume,tv_language,tv_setting,tv_setprepare,tv_time,tv_notification;
    TextView tv_set_sound,tv_sound_type,tv_other_setting;
    SeekBar sk;
    ImageView im_backbutton;
    Locale myLocale;
    EditText et_prepare;
    AudioManager audioManager;
    Spinner spinnerlang,spinner_sound;
    int lang_chng;
    int p;
    int position,position_sound;
    public static final int INTERVAL_DAY = 24 * 60 * 60 * 1000;
    String time,am_pm;
    int tempHR;
    String sound_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        context         =  this;
        im_backbutton   =  (ImageView)findViewById(R.id.im_backbutton);
        sound           =  (ToggleButton)findViewById(R.id.toggleButton_Sound);
        sec_sound       =  (ToggleButton)findViewById(R.id.toggleButton_SecSound);
        display         =  (ToggleButton)findViewById(R.id.toggleButton_diaplay);
        vib             =  (ToggleButton)findViewById(R.id.toggleButton_Vibration);
        noti            =  (ToggleButton)findViewById(R.id.toggleButton_reminder);
        sk              =  (SeekBar)findViewById(R.id.seekBar_volume);
        et_prepare      =  (EditText)findViewById(R.id.et_prepare);

        tv_sound        =  (TextView)findViewById(R.id.tv_sound);
        tv_sound_sec    =  (TextView)findViewById(R.id.tv_sound_sec);
        tv_display      =  (TextView)findViewById(R.id.tv_display);
        tv_vibration    =  (TextView)findViewById(R.id.tv_vibration);
        tv_volume       =  (TextView)findViewById(R.id.tv_volume);
        tv_language     =  (TextView)findViewById(R.id.tv_language);
        tv_setting      =  (TextView)findViewById(R.id.tv_anulom);
        tv_setprepare   =  (TextView)findViewById(R.id.tv_setprepare);
        tv_time         =  (TextView)findViewById(R.id.tv_time);
        tv_notification =  (TextView)findViewById(R.id.tv_notification);
        spinnerlang     =  (Spinner)findViewById(R.id.spinnerlang);
        spinner_sound   =  (Spinner)findViewById(R.id.spinnersound);

        tv_set_sound    = (TextView)findViewById(R.id.tv_set_sound);
        tv_sound_type   = (TextView)findViewById(R.id.tv_sound_type);
        tv_other_setting= (TextView)findViewById(R.id.tv_other_setting);

        sound.setChecked(GlobalClass.callSavedPreferences1("sound", context));
        sec_sound.setChecked(GlobalClass.callSavedPreferences1("second_sound", context));
        display.setChecked(GlobalClass.callSavedPreferences1("display", context));
        vib.setChecked(GlobalClass.callSavedPreferences1("vibration", context));
        noti.setChecked(GlobalClass.callSavedPreferences1("notification", context));

        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//Language Spinner
        final ArrayList<String> list=new ArrayList<String>();
        list.add("English");
        list.add("हिंदी");
        list.add("русский");
        list.add("Français");
        list.add("Deutsche");
        list.add("Español");

        ArrayAdapter<String> ad=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerlang.setAdapter(ad);
        spinnerlang.setSelection(GlobalClass.callSavedPreferences2("language", position,context));

        final ArrayList<String> list_sound=new ArrayList<String>();
        list_sound.add("Voice");
        list_sound.add("Tone");

        ArrayAdapter<String> ad_sound=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list_sound);
        ad_sound.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sound.setAdapter(ad_sound);
        position_sound=GlobalClass.callSavedPreferences2("spinnerSound", position_sound, context);
        spinner_sound.setSelection(position_sound);

        changeLang();

        if (GlobalClass.callSavedPreferences1("notification", context))
            setTimetoText();
        else
            tv_time.setVisibility(View.INVISIBLE);

        im_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        noti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChacked) {
                if (isChacked)
                    setalarm(GlobalClass.callSavedPreferences2("hr",0,context), GlobalClass.callSavedPreferences2("min",0,context));
                else
                    cancelAlarm();
                GlobalClass.savePreferences1("notification",isChacked, context);
            }
        });

        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set_timer_dialog();
            }
        });

//for total sound of app
        sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    GlobalClass.savePreferences1("sound", isChecked, context);
                    GlobalClass.savePreferences1("second_sound", isChecked, context);
                    sec_sound.setChecked(isChecked);
            }
        });

//for every second sound
        sec_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChacked) {
                    GlobalClass.savePreferences1("second_sound", isChacked, context);
            }
        });

        //for vibration with sound
        vib.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChacked) {
                    GlobalClass.savePreferences1("vibration", isChacked, context);
            }
        });

//keep display on setting during the app on
        display.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChacked) {
                GlobalClass.savePreferences1("display", isChacked, context);
                if (isChacked)
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                else
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });

        //Change the language according to spinner
        spinnerlang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item=parent.getItemAtPosition(position).toString();
                String str_lang="";
                position = spinnerlang.getSelectedItemPosition();
                switch (position){
                    case 0:
                        str_lang="en";
                        break;
                    case 1:
                        str_lang="hi";
                        break;
                    case 2:
                        str_lang="ru";
                        break;
                    case 3:
                        str_lang="fr";
                        break;
                    case 4:
                        str_lang="de";
                        break;
                    case 5:
                        str_lang="es";
                        break;
                    default:
                        str_lang="en";
                        break;
                }

                myLocale = new Locale(str_lang);
                Locale.setDefault(myLocale);
                android.content.res.Configuration config = new android.content.res.Configuration();
                config.locale = myLocale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                updateTexts();
                GlobalClass.savePreferences2("language", position,context);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


     /*   spinner_sound.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item=parent.getItemAtPosition(position).toString();
                position = spinner_sound.getSelectedItemPosition();
                if(item.equals(getString(R.string.Voice))){
                    sound_type = "Voice";
                    GlobalClass.savePreferences("Sound_type",sound_type,context);
                    GlobalClass.savePreferences2("spinnerSound", position,context);
                    Log.e("sound type",sound_type);
                }
                else if(item.equals(getString(R.string.Tone)))
                {
                    sound_type = "Tone";
                    Log.e("sound type",sound_type);
                    GlobalClass.savePreferences("Sound_type",sound_type,context);
                    GlobalClass.savePreferences2("spinnerSound", position,context);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //sound_type="Voice";
                //GlobalClass.savePreferences("Sound_type",sound_type,context);
            }
        });*/

        try
        {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            sk.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            sk.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
                {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void set_timer_dialog(){
        final Calendar c = Calendar.getInstance();
        int hr = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(SettingActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        int tempHR=hourOfDay;
                        String time,am_pm;

                        if(tempHR < 12 && tempHR >= 0)
                        {
                            if(tempHR==0)
                                tempHR=12;
                            am_pm=" AM";
                            time = String.format("%02d : %02d", tempHR, minute);
                            tv_time.setText(time+am_pm);
                        }
                        else {
                            tempHR -= 12;
                            if(tempHR == 0)
                            {
                                tempHR = 12;
                            }
                            am_pm=" PM";
                            time = String.format("%02d : %02d", tempHR, minute);
                            tv_time.setText(time+am_pm);
                        }

                        Calendar calendar = new GregorianCalendar();
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        calendar.set(Calendar.SECOND, 0);
                        calendar.set(Calendar.MILLISECOND, 0);

                        setalarm(hourOfDay,minute);
                    }
                }, hr, min, false);
        timePickerDialog.show();
    }

    public void changeLang() {
        lang_chng = GlobalClass.callSavedPreferences2("language", lang_chng, context);
        String str_lang = "";
        switch (lang_chng){
            case 0:
                str_lang="en";
                break;
            case 1:
                str_lang="hi";
                break;
            case 2:
                str_lang="ru";
                break;
            case 3:
                str_lang="fr";
                break;
            case 4:
                str_lang="de";
                break;
            case 5:
                str_lang="es";
                break;
            default:
                str_lang="en";
                break;
        }
        myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts(){
        tv_sound.setText(R.string.Sound);
        tv_sound_sec.setText(R.string.Sound_Sec);
        tv_vibration.setText(R.string.Vibration);
        tv_display.setText(R.string.Display);
        tv_volume.setText(R.string.Volume);
        tv_language.setText(R.string.Language);
        tv_setting.setText(R.string.Setting);
        tv_setprepare.setText(R.string.Set_Preparation_Time);
        tv_notification.setText(R.string.Notification);
        tv_set_sound.setText(R.string.Set_sound);
        tv_sound_type.setText(R.string.Sound_Type);
        tv_other_setting.setText(R.string.Other_settings);

       /* final ArrayList<String> list_sound=new ArrayList<String>();
        list_sound.add(getResources().getString(R.string.Voice));
        list_sound.add(getResources().getString(R.string.Tone));

        ArrayAdapter<String> ad_sound=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list_sound);
        ad_sound.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sound.setAdapter(ad_sound);
        position_sound=GlobalClass.callSavedPreferences2("spinnerSound", position_sound, context);
        spinner_sound.setSelection(position_sound);*/
    }

    public void setTimetoText(){
        tempHR     = GlobalClass.callSavedPreferences2("hr", 0, context);
        int minute = GlobalClass.callSavedPreferences2("min", 0, context);
        String time,am_pm;
        if(tempHR < 12 && tempHR >= 0)
        {
            if(tempHR==0)
                tempHR=12;
            am_pm=" AM";
            time = String.format("%02d : %02d", tempHR, minute);
            tv_time.setText(time+am_pm);
        }
        else {
            tempHR -= 12;
            if(tempHR == 0)
            {
                tempHR = 12;
            }
            am_pm=" PM";
            time = String.format("%02d : %02d", tempHR, minute);
            tv_time.setText(time+am_pm);
        }
    }

    public void setalarm(int hr, int min){
        tempHR=hr;
        GlobalClass.savePreferences2("hr", hr, context);
        GlobalClass.savePreferences2("min", min, context);
        GlobalClass.savePreferences2("alarm_set", 1, context);

        if(tempHR < 12 && tempHR >= 0)
        {
            if(tempHR==0)
                tempHR=12;
            am_pm=" AM";
            time = String.format("%02d : %02d", tempHR, min);
            tv_time.setText(time+am_pm);
        }
        else {
            tempHR -= 12;
            if(tempHR == 0)
            {
                tempHR = 12;
            }
            am_pm=" PM";
            time = String.format("%02d : %02d", tempHR, min);
            tv_time.setText(time+am_pm);
        }

        tv_time.setVisibility(View.VISIBLE);
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hr);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
        Intent intent   = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        long triggerMillis = calendar.getTimeInMillis();

        if (calendar.getTimeInMillis() < Calendar.getInstance()
                .getTimeInMillis()) {
            triggerMillis = calendar.getTimeInMillis() + INTERVAL_DAY;

            System.out.println("Alarm will go off next day");
        }

        am.setRepeating(AlarmManager.RTC_WAKEUP, triggerMillis,AlarmManager.INTERVAL_DAY, pendingIntent);
        Log.e("set alarm costum",time + am_pm);
    }

    //Alarm Cancel
    public void cancelAlarm(){
        tv_time.setVisibility(View.INVISIBLE);
        AlarmManager am = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    public void onBackPressed(){
        super.onBackPressed();
        String abc = et_prepare.getText().toString();
        if (abc.equals("")) {
            et_prepare.setText("0");
            p = 0;
            GlobalClass.savePreferences2("preparation", p, context);
        }
        else {
            p = Integer.parseInt(String.valueOf(abc));
            GlobalClass.savePreferences2("preparation", p, context);
        }
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }//close method

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
