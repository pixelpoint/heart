package com.habit;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.habit.data.GlobalClass;

/**
 * Created by Hitesh on 21-Jun-16.
 */
public class Sounds {
    MediaPlayer mp;
    Boolean sound,sec_sound,vibration;
    Vibrator v;
    String sound_type;
    int lang_chng;
    Context context;

    public Sounds(Context context) {
        this.context = context;
        v           = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        sound       = GlobalClass.callSavedPreferences1("sound", context);
        sec_sound   = GlobalClass.callSavedPreferences1("second_sound", context);
        vibration   = GlobalClass.callSavedPreferences1("vibration", context);
        sound_type  = GlobalClass.callSavedPreferences("Sound_type",context);
        lang_chng   = GlobalClass.callSavedPreferences2("spinnerSelection", lang_chng, context);
    }

    public void playSound1() {
        try {
            mp = MediaPlayer.create(context, (R.raw.bell_temple_6));
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sec_sound && mp != null)
                        mp.start();
                }
            });

            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();

        }
    }

    public void playSound2() {
        try {
            mp = MediaPlayer.create(context, (R.raw.change));
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp !=null)
                        mp.start();
                    if (vibration)
                        v.vibrate(500);
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void playSound3() {
        try {
            mp = MediaPlayer.create(context, (R.raw.finish));
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp != null)
                        mp.start();
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void playSoundExhaelKapal() {
        try {
            mp = MediaPlayer.create(context, (R.raw.exhale));
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp != null)
                        mp.start();
                    if (vibration)
                        v.vibrate(500);
                }
            });

            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();

        }
    }

    public void inhale_sound() {
        try {

            if (sound_type.equals("Tone"))
                mp = MediaPlayer.create(context, (R.raw.change));
            else {
                if (lang_chng == 1)
                    mp = MediaPlayer.create(context, (R.raw.inhale_hindi));
                else
                    mp = MediaPlayer.create(context, (R.raw.inhale));
                }

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp !=null)
                        mp.start();
                    if (vibration)
                        v.vibrate(500);
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void hold_sound() {
        try {

            if (sound_type.equals("Tone"))
                mp = MediaPlayer.create(context, (R.raw.change));
            else{
                if (lang_chng == 1)
                    mp = MediaPlayer.create(context, (R.raw.hold_hindi));
                else
                    mp = MediaPlayer.create(context, (R.raw.hold));
            }

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp !=null)
                        mp.start();
                    if (vibration)
                        v.vibrate(500);
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void exhale_sound() {
        try {

            if (sound_type.equals("Tone"))
                mp = MediaPlayer.create(context, (R.raw.change));
            else {
                if (lang_chng == 1)
                    mp = MediaPlayer.create(context, (R.raw.exhale_hindi));
                else
                    mp = MediaPlayer.create(context, (R.raw.exhale_normal));

            }

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp !=null)
                        mp.start();
                    if (vibration)
                        v.vibrate(500);
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public Boolean isMediaPlayerRunning(){
        if (mp.isPlaying()){
            return true;
        }
        else
            return false;
    }
}
