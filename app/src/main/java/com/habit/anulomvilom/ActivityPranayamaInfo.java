package com.habit.anulomvilom;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.habit.R;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;

import java.util.Locale;

public class ActivityPranayamaInfo extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    Context context;
    Boolean display;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    ImageView imbackbutton;
    TextView tv_setu;
    int type=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_setu_bandhasana);

        context         =this;
        imbackbutton    =(ImageView) findViewById(R.id.im_backbutton);
        viewPager       = (ViewPager) findViewById(R.id.pager_setu);
        tabLayout       = (TabLayout) findViewById(R.id.tab_setu);
        tv_setu         = (TextView) findViewById(R.id.tv_anulom);

        //changeLang();

        if (getIntent().getExtras()!=null)
            type = getIntent().getIntExtra("type",type);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.About));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Benefits));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Steps));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tv_setu.setText(R.string.Help);

        Pager_PranayamaInfo adapter = new Pager_PranayamaInfo(getSupportFragmentManager(), tabLayout.getTabCount(),type);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(this);

        imbackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /*public void changeLang() {
        String str_lang = "";
        if (lang_chng == 1) {
            str_lang = "hi";
        } else if(lang_chng == 2) {
            str_lang = "ru";
        } else if(lang_chng == 3) {
            str_lang = "fr";
        }
        else if (lang_chng == 4)
            str_lang = "de";
        else if (lang_chng == 5)
            str_lang = "es";
        else
            str_lang = "en";
        myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_setu.setText(R.string.Setu);
    }*/

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onResume(){
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

}
