package com.habit.anulomvilom;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.habit.MyNumberPicker;
import com.habit.R;
import com.habit.Sounds;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;
import com.habit.kapalbhati.Kapalbhati_Activity;
import com.habit.report.ReportActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class Anulom_Activity extends AppCompatActivity {
//Reference Variable
    ImageView im_data, im_back1, im_forword1, im_inhel, im_pause, im_backbutton, tv_start, tv_stop,iv_reset,iv_help;
    TextView tv_anulom,tv_inhalechange, tv_inhaltimer, tv_timerhr, tv_timermin, tv_timersec, tv_timerhr1, tv_timermin1, tv_timersec1,tv_inhale_h,tv_hold_h,tv_exhale_h,tv_round_h,tv_timer_h,tv_action_h,tv_hol_h;
    LinearLayout ll_start, ll_stop,ll_total,content_anulom;
    MyNumberPicker np_inhale,np_hold,np_exhale,np_holdafter,np_rounds;
    RelativeLayout rl_instruction;
    Button bt_instruction;
    Context con;

    //Values of Inhale,Hold and Exhale
    int i = 1,h = 4,e = 8,r = 1;
    int m=0,se=0;

    //Set Timer
    int hr, min, sec, tempsec, tt;
    String time;
    int tempI, tempH, tempE = 3;
    int loop = 2;
    int flag = 0;
    int p;
    int tempP=p;
    int valuechange,btnanulom;
    int hafter,tempHA;
    Thread t;
    Sounds sounds;
   // int lang_chng;
    int totaltime;
    int habit_start=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_anulom);

        con                 = this;
        im_data             = (ImageView) findViewById(R.id.im_data);
        im_back1            = (ImageView) findViewById(R.id.im_back1);
        im_forword1         = (ImageView) findViewById(R.id.im_forward1);
        im_inhel            = (ImageView) findViewById(R.id.im_inhale);
        im_pause            = (ImageView) findViewById(R.id.im_pause);
        iv_help             = (ImageView) findViewById(R.id.iv_help);
        im_backbutton       = (ImageView) findViewById(R.id.im_backbutton);
        tv_timerhr          = (TextView) findViewById(R.id.tv_timerhr);
        tv_timermin         = (TextView) findViewById(R.id.tv_timermin);
        tv_timersec         = (TextView) findViewById(R.id.tv_timersec);
        tv_timerhr1         = (TextView) findViewById(R.id.tv_timerhr1);
        tv_timermin1        = (TextView) findViewById(R.id.tv_timermin1);
        tv_timersec1        = (TextView) findViewById(R.id.tv_timersec1);
        tv_inhalechange     = (TextView) findViewById(R.id.tv_inhalechange);
        tv_inhaltimer       = (TextView) findViewById(R.id.tv_inhaletimer);
        tv_anulom           = (TextView) findViewById(R.id.tv_anulom);
        tv_inhale_h         = (TextView) findViewById(R.id.tv_inhale_h);
        tv_hold_h           = (TextView) findViewById(R.id.tv_hold_h);
        tv_exhale_h         = (TextView) findViewById(R.id.tv_exhale_h);
        tv_round_h          = (TextView) findViewById(R.id.tv_round_h);
        tv_timer_h          = (TextView) findViewById(R.id.tv_timer_h);
        tv_action_h         = (TextView) findViewById(R.id.tv_action_h);
        tv_hol_h            = (TextView) findViewById(R.id.tv_hol_h);
        tv_start            = (ImageView) findViewById(R.id.tv_start);
        tv_stop             = (ImageView) findViewById(R.id.tv_stop);
        ll_start            = (LinearLayout) findViewById(R.id.ll_start);
        ll_stop             = (LinearLayout) findViewById(R.id.ll_stop);

        iv_reset            = (ImageView) findViewById(R.id.iv_reset);
        ll_total            = (LinearLayout) findViewById(R.id.ll_total);

        np_inhale           = (MyNumberPicker) findViewById(R.id.np_inhale);
        np_hold             = (MyNumberPicker) findViewById(R.id.np_hold);
        np_exhale           = (MyNumberPicker) findViewById(R.id.np_exhale);
        np_holdafter        = (MyNumberPicker) findViewById(R.id.np_holdafter);
        np_rounds           = (MyNumberPicker) findViewById(R.id.np_rounds);

        content_anulom      = (LinearLayout) findViewById(R.id.content_anulom);
        sounds              = new Sounds(con);

        p                   = GlobalClass.callSavedPreferences2("preparation", p, con);
        habit_start         = GlobalClass.callSavedPreferences2("habit_start",habit_start,con);

        //call numberpicker function
        //  changeLang();
        numberPick();

//For change the value of Inhale,Hold and Exhale
        im_forword1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i < 50) {
                    i = i + 1;
                    e = i * 2;
                    h = e * 2;
                    setStartTime();
                    saveSharepreference();
                    GlobalClass.savePreferences2("btnanulom", 2, con);
                    numberPick();
                } else if (i == 50) {
                    e = i * 2;
                    h = e * 2;
                    setStartTime();
                    saveSharepreference();
                    GlobalClass.savePreferences2("btnanulom", 2, con);
                    numberPick();
                }
            }
        });

//Reduced the value of inhale,hold and exhale simultaneously
        im_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (i > 1) {
                    i = i - 1;
                    e = i * 2;
                    h = e * 2;
                    setStartTime();
                    saveSharepreference();
                    GlobalClass.savePreferences2("btnanulom", 2, con);
                    numberPick();
                } else if (i == 1) {
                    e = i * 2;
                    h = e * 2;
                    setStartTime();
                    saveSharepreference();
                    GlobalClass.savePreferences2("btnanulom", 2, con);
                    numberPick();
                }
            }
        });


//Help Button
        iv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetValue();
            }
        });

        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(con,ActivityPranayamaInfo.class);
                intent.putExtra("type",1);
                startActivity(intent);
            }
        });

//Start Button Functionality
        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });

//Stop Button
        tv_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                restart();
                flag = 0;
            }
        });
//Pause Button
        im_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                tv_stop.setVisibility(View.GONE);
                tv_start.setVisibility(View.VISIBLE);
                flag = 1;
            }
        });
//for database
        im_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(con, ReportActivity.class);
                in.putExtra("type",1);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
//help
        im_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

//for set the time
    public void setStartTime() {
        tempsec = ((i + h + e + hafter) * 2 * r);
        hr      = tempsec / 3600;
        int v   = tempsec % 3600;
        min     = v / 60;
        sec     = v % 60;
        time    = String.format("  %02d     %02d     %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

//Change the time
    public void setCountDownTime() {
        hr     = tempsec / 3600;
        int v  = tempsec % 3600;
        min    = v / 60;
        sec    = v % 60;
        time   = String.format("  %02d     %02d     %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

//Reset the all contents when complete or stop
    public void restart() {

        loop = 2;
        tempP=p;
        m=0;
        setEnabled();

        int doTime = tt - tempsec;

        totaltime = doTime+totaltime;

        setStartTime();
        if(doTime!=0){
        DatabaseHandler db = new DatabaseHandler(con);
        //db.addContact(i,h,e, r, 1, tt, doTime);
        db.addPranayamaTimes(String.format("%2d:%2d:%2d", i, h, e), r, 1, tt, doTime);
        }
        m=0;

        if (habit_start==1){
           startActivity(new Intent(con, Kapalbhati_Activity.class));
        }
    }

    public void play(){
        setDisabled();
        m=1;
        if (flag == 0) {
            tempI = i;
            tempP=p;
            tt = tempsec;
        }
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCountDownTime();
                                if (tempsec > 0) {
                                    if (loop % 2 == 0) {

                                        //if preparation time not zero then first start preparation
                                        if(tempP <=p && tempP>0){
                                            sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Prepare);
                                            tv_inhaltimer.setText(String.valueOf(tempP));

                                            tempP = tempP - 1;
                                            if (tempP == 0)
                                                tempI= i;
                                        }
                                        //if preparation time zero then start inhale
                                        else if (tempI <= i && tempI > 0) {
                                            if (tempI == i)
                                                sounds.inhale_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Inhale_Left);
                                            tv_inhaltimer.setText(String.valueOf(tempI));
                                            im_inhel.setImageResource(R.drawable.action_inhale_left);

                                            tempI = tempI - 1;

                                            if (tempI == 0) {
                                                if (h!=0)
                                                    tempH = h;
                                                else
                                                    tempE=e;
                                            }
                                            se=1;

                                            //when inhale become zero then hold start
                                        } else if (tempH <= h && tempH > 0) {

                                            if (tempH == h)
                                                sounds.hold_sound();
                                            else
                                                sounds.playSound1();

                                            tv_inhalechange.setText(R.string.Hold_Hold);
                                            tv_inhaltimer.setText(String.valueOf(tempH));
                                            im_inhel.setImageResource(R.drawable.action_no);

                                            tempH = tempH - 1;
                                            if (tempH == 0)
                                                tempE = e;
                                            se=1;

                                            //when hold become zero then exhate start
                                        } else if (tempE <= e && tempE > 0) {
                                            if (tempE == e)
                                                sounds.exhale_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Exhale_Right);
                                            tv_inhaltimer.setText(String.valueOf(tempE));
                                            im_inhel.setImageResource(R.drawable.action_exhale_right);

                                            tempE = tempE - 1;
                                            se=1;
                                            if (tempE == 0) {
                                                if(hafter !=0)
                                                    tempHA = hafter;
                                                else{
                                                    tempI=i;
                                                    loop = loop-1;
                                                }
                                            }

                                        } else if(tempHA <= hafter && tempHA > 0){

                                            if (tempHA == hafter)
                                                sounds.hold_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Hold_Hold);
                                            tv_inhaltimer.setText(String.valueOf(tempHA));
                                            im_inhel.setImageResource(R.drawable.action_no);


                                            tempHA = tempHA - 1;
                                            se=1;
                                            if (tempHA == 0) {
                                                tempI = i;
                                                //loop for half round of anulom vilom
                                                loop = loop - 1;
                                            }
                                        }


                                    } else {

                                        //when half round complete then again start from inhale but inhale right
                                        if (tempI <= i && tempI > 0) {
                                            if (tempI == i)
                                                sounds.inhale_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Inhale_Right);
                                            tv_inhaltimer.setText(String.valueOf(tempI));
                                            im_inhel.setImageResource(R.drawable.action_inhale_right);

                                            tempI = tempI - 1;
                                            se=1;
                                            if (tempI == 0) {
                                                if (h!=0)
                                                    tempH = h;
                                                else
                                                    tempE=e;
                                            }

                                        } else if (tempH <= h && tempH > 0) {

                                            if (tempH == h)
                                                sounds.hold_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Hold_Hold);
                                            tv_inhaltimer.setText(String.valueOf(tempH));
                                            im_inhel.setImageResource(R.drawable.action_no);

                                            tempH = tempH - 1;
                                            if (tempH == 0)
                                                tempE = e;
                                            se=1;

                                        } else if (tempE <= e && tempE > 0) {

                                            if (tempE == e)
                                                sounds.exhale_sound();
                                            else
                                                sounds.playSound1();

                                            tv_inhalechange.setText(R.string.Exhale_Left);
                                            tv_inhaltimer.setText(String.valueOf(tempE));
                                            im_inhel.setImageResource(R.drawable.action_exhale_left);

                                            tempE = tempE - 1;
                                            se=1;
                                            if (tempE == 0) {
                                                if(hafter !=0)
                                                    tempHA = hafter;
                                                else{
                                                    tempI=i;
                                                    loop = loop-1;
                                                }
                                            }
                                        }

                                        else if(tempHA <= hafter && tempHA >0){

                                            if (tempHA == hafter)
                                                sounds.hold_sound();
                                            else
                                                sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Hold_Hold);
                                            tv_inhaltimer.setText(String.valueOf(tempHA));
                                            im_inhel.setImageResource(R.drawable.action_no);

                                            tempHA = tempHA - 1;
                                            se=1;
                                            if (tempHA == 0) {
                                                tempI = i;
                                                //loop for half round of anulom vilom
                                                loop = loop - 1;
                                            }
                                        }

                                    }  // round if.........

                                }// outer if close
                                else {
                                    sounds.playSound3();
                                    t.interrupt();
                                    restart();
                                }
                                if(se==1){
                                    tempsec = tempsec - 1;
                                    se=0;
                                }
                            }  // run close

                        }); // Runnable close
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }
//Method for change the colour and disable the arrow of inhale and rounds
    public void setDisabled() {
        tv_start.setVisibility(View.GONE);
        tv_stop.setVisibility(View.VISIBLE);
        ll_stop.setVisibility(View.GONE);
        ll_start.setVisibility(View.VISIBLE);
        iv_reset.setVisibility(View.INVISIBLE);
        im_data.setVisibility(View.INVISIBLE);
        im_backbutton.setVisibility(View.INVISIBLE);
        im_pause.setVisibility(View.VISIBLE);
        iv_help.setVisibility(View.GONE);
    }

//Method for change the colour and enable the arrow of inhale and rounds
    public void setEnabled() {

        im_inhel.setImageBitmap(null);
        tv_inhalechange.setText("");
        tv_inhaltimer.setText("");
        tv_start.setVisibility(View.VISIBLE);
        tv_stop.setVisibility(View.GONE);
        ll_start.setVisibility(View.GONE);
        ll_stop.setVisibility(View.VISIBLE);

        im_backbutton.setVisibility(View.VISIBLE);
        iv_reset.setVisibility(View.VISIBLE);
        im_data.setVisibility(View.VISIBLE);
        im_pause.setVisibility(View.GONE);
        iv_help.setVisibility(View.VISIBLE);
    }

//reset all inhale hold exhale andr round value on click reset button
    public void resetValue(){
        i = 1;
        h = 4;
        e = 2;
        r = 1;
        hafter=0;
        setStartTime();
        saveSharepreference();
        numberPick();
    }

    public void numberPick(){
//make divider transparent
        setDividerColor(np_inhale, Color.TRANSPARENT);
        setDividerColor(np_hold, Color.TRANSPARENT);
        setDividerColor(np_exhale, Color.TRANSPARENT);
        setDividerColor(np_holdafter, Color.TRANSPARENT);
        setDividerColor(np_rounds, Color.TRANSPARENT);
        valuechange  =GlobalClass.callSavedPreferences2("valuechange",valuechange,con);
        btnanulom    =GlobalClass.callSavedPreferences2("btnanulom",btnanulom,con);

       if(valuechange==2 || btnanulom == 2) {
           i      = GlobalClass.callSavedPreferences2("inhale", i, con);
           h      = GlobalClass.callSavedPreferences2("hold", h, con);
           e      = GlobalClass.callSavedPreferences2("exhale", e, con);
           r      = GlobalClass.callSavedPreferences2("round", r, con);
           hafter = GlobalClass.callSavedPreferences2("hafter", hafter, con);
       }
        else{
           i=1;
           h=4;
           e=2;
           r=1;
           hafter=0;
           saveSharepreference();
       }
//set minimum and maximum values
        np_inhale.setMinValue(1);
        np_inhale.setMaxValue(50);
        np_hold.setMinValue(0);
        np_hold.setMaxValue(200);
        np_exhale.setMinValue(1);
        np_exhale.setMaxValue(100);
        np_holdafter.setMinValue(0);
        np_holdafter.setMaxValue(10);
        np_rounds.setMinValue(1);
        np_rounds.setMaxValue(50);

        np_inhale.setValue(i);
        np_hold.setValue(h);
        np_exhale.setValue(e);
        np_holdafter.setValue(hafter);
        np_rounds.setValue(r);

        setStartTime();

        np_inhale.setWrapSelectorWheel(true);
        np_inhale.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=newVal;
                h=np_hold.getValue();
                e=np_exhale.getValue();
                hafter= np_holdafter.getValue();
                r=np_rounds.getValue();
                saveSharepreference();
                GlobalClass.savePreferences2("valuechange",2,con);
                setStartTime();
            }
        });

        np_hold.setWrapSelectorWheel(true);
        np_hold.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                h=newVal;
                e=np_exhale.getValue();
                hafter= np_holdafter.getValue();
                r=np_rounds.getValue();
                saveSharepreference();
                GlobalClass.savePreferences2("valuechange",2,con);
                setStartTime();
            }
        });

        np_exhale.setWrapSelectorWheel(true);
        np_exhale.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                h=np_hold.getValue();
                e=newVal;
                hafter= np_holdafter.getValue();
                r=np_rounds.getValue();
                saveSharepreference();
                GlobalClass.savePreferences2("valuechange",2,con);
                setStartTime();
            }
        });

        np_holdafter.setWrapSelectorWheel(true);
        np_holdafter.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                h=np_hold.getValue();
                e=np_exhale.getValue();
                hafter= newVal;
                r=np_rounds.getValue();
                saveSharepreference();
                GlobalClass.savePreferences2("valuechange",2,con);
                setStartTime();
            }
        });

        np_rounds.setWrapSelectorWheel(true);
        np_rounds.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                h=np_hold.getValue();
                e=np_exhale.getValue();
                hafter= np_holdafter.getValue();
                r=newVal;
                saveSharepreference();
                GlobalClass.savePreferences2("valuechange",2,con);
                setStartTime();
            }
        });
    }

    public void saveSharepreference(){
        GlobalClass.savePreferences2("inhale", i, con);
        GlobalClass.savePreferences2("hold", h, con);
        GlobalClass.savePreferences2("exhale", e, con);
        GlobalClass.savePreferences2("round", r, con);
        GlobalClass.savePreferences2("hafter",hafter,con);

    }

    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void changeLang() {
        String str_lang = "";
        switch (GlobalClass.callSavedPreferences2("language", 0, con)){
            case 0:
                str_lang="en";
                break;
            case 1:
                str_lang="hi";
                break;
            case 2:
                str_lang="ru";
                break;
            case 3:
                str_lang="fr";
                break;
            case 4:
                str_lang="de";
                break;
            case 5:
                str_lang="es";
                break;
            default:
                str_lang="en";
                break;
        }
        Locale myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_inhale_h.setText(R.string.Inhale);
        tv_hold_h.setText(R.string.Hold);
        tv_exhale_h.setText(R.string.Exhale);
        tv_round_h.setText(R.string.Rounds);
        tv_timer_h.setText(R.string.Total_Time);
        tv_action_h.setText(R.string.Action);
        tv_anulom.setText(R.string.AnulomVilom);
        tv_hol_h.setText(R.string.Hold);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
       /* if(m==1){
            t.interrupt();
            tv_stop.setVisibility(View.GONE);
            tv_start.setVisibility(View.VISIBLE);
            flag = 1;
        }
        else{}*/
    }
    @Override
    public void onResume(){
        super.onResume();
        sounds     = new Sounds(con);
        changeLang();
        if (GlobalClass.callSavedPreferences1("display", con))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
    public void onBackPressed() {
        if(m==1)
            t.interrupt();
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        GlobalClass.savePreferences2("habit_start",0,con);
    }
}