package com.habit.anulomvilom;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.habit.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Benefits_Pranayama.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Benefits_Pranayama#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Benefits_Pranayama extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int type;

    private OnFragmentInteractionListener mListener;

    public Benefits_Pranayama(int type) {
        // Required empty public constructor
        this.type=type;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Benefits_Pranayama.
     */
    // TODO: Rename and change types and number of parameters
    public static Benefits_Pranayama newInstance(String param1, String param2,int type) {
        Benefits_Pranayama fragment = new Benefits_Pranayama(type);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup.LayoutParams lp     = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ViewGroup.LayoutParams linear = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        FrameLayout mainLayout    = new FrameLayout(getActivity());
        mainLayout.setLayoutParams(lp);
        mainLayout.setPadding(10,10,10,10);

        ScrollView scrollView     = new ScrollView(getActivity());
        scrollView.setLayoutParams(lp);

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(linear);
        linearLayout.setPadding(0,0,0,20);
        linearLayout.setBackgroundResource(R.drawable.stroke);

        mainLayout.addView(scrollView);
        scrollView.addView(linearLayout);

        TextView heading_benefit  = new TextView(getActivity());
        heading_benefit.setTextColor(Color.RED);
        heading_benefit.setTextSize(20);
        heading_benefit.setPadding(40,20,0,20);

        TextView tv_content_benefit = new TextView(getActivity());
        tv_content_benefit.setPadding(60,10,20,0);
        tv_content_benefit.setTextColor(Color.BLACK);

        TextView heading_precaution  = new TextView(getActivity());
        heading_precaution.setTextColor(Color.RED);
        heading_precaution.setTextSize(20);
        heading_precaution.setPadding(40,20,0,20);

        TextView tv_content_pre = new TextView(getActivity());
        tv_content_pre.setPadding(60,10,20,0);
        tv_content_pre.setTextColor(Color.BLACK);

        switch (type){
            case 1:
                heading_benefit.setText(R.string.Benefit_Anulom);
                tv_content_benefit.setText(getResources().getString(R.string.Benefit_Anulom1)+"\n\n"+getResources().getString(R.string.Benefit_Anulom2)+"\n\n"+getResources().getString(R.string.Benefit_Anulom3)+"\n\n"+getResources().getString(R.string.Benefit_Anulom4)+"\n\n"+getResources().getString(R.string.Benefit_Anulom5)+"\n\n"+getResources().getString(R.string.Benefit_Anulom6)+"\n\n"+getResources().getString(R.string.Benefit_Anulom7)+"\n\n"+getResources().getString(R.string.Benefit_Anulom8)+"\n\n"+getResources().getString(R.string.Benefit_Anulom9)+"\n\n"+getResources().getString(R.string.Benefit_Anulom10)+"\n\n"+getResources().getString(R.string.Benefit_Anulom11));
                heading_precaution.setText(R.string.Precautions);
                tv_content_pre.setText(getResources().getString(R.string.PRecaution_Anulom1)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom2)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom3)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom4));
                break;
            case 2:
                heading_benefit.setText(R.string.Benefit_Kapal);
                tv_content_benefit.setText(getResources().getString(R.string.Benefit_Kapal1)+"\n\n"+getResources().getString(R.string.Benefit_Kapal2)+"\n\n"+getResources().getString(R.string.Benefit_Kapal3)+"\n\n"+getResources().getString(R.string.Benefit_Kapal4)+"\n\n"+getResources().getString(R.string.Benefit_Kapal5)+"\n\n"+getResources().getString(R.string.Benefit_Kapal6)+"\n\n"+getResources().getString(R.string.Benefit_Kapal7)+"\n\n"+getResources().getString(R.string.Benefit_Kapal8)+"\n\n"+getResources().getString(R.string.Benefit_Kapal9)+"\n\n"+getResources().getString(R.string.Benefit_Kapal10)+"\n\n"+getResources().getString(R.string.Benefit_Kapal11)+"\n\n"+getResources().getString(R.string.Benefit_Kapal12));
                heading_precaution.setText(R.string.Precautions);
                tv_content_pre.setText(getResources().getString(R.string.Precaution_Kapal1)+"\n\n"+getResources().getString(R.string.Precaution_Kapal2)+"\n\n"+getResources().getString(R.string.Precaution_Kapal3)+"\n\n"+getResources().getString(R.string.Precaution_Kapal4)+"\n\n"+getResources().getString(R.string.Precaution_Kapal5));
                break;
            case 3:
                heading_benefit.setText(R.string.Benefit_Bhramari);
                tv_content_benefit.setText(getResources().getString(R.string.Benefit_Bhramari1)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari2)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari3)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari4)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari5)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari6)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari7)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari8)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari9)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari10)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari11)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari12)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari13)+"\n\n"+getResources().getString(R.string.Benefit_Bhramari14));
                heading_precaution.setText(R.string.Precautions);
                tv_content_pre.setText(getResources().getString(R.string.Precaution_Bhramari1)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari2)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari3)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari4)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari5)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari6)+"\n\n"+getResources().getString(R.string.Precaution_Bhramari7));
                break;
            default:
                heading_benefit.setText(R.string.Benefit_Anulom);
                tv_content_benefit.setText(getResources().getString(R.string.Benefit_Anulom1)+"\n\n"+getResources().getString(R.string.Benefit_Anulom2)+"\n\n"+getResources().getString(R.string.Benefit_Anulom3)+"\n\n"+getResources().getString(R.string.Benefit_Anulom4)+"\n\n"+getResources().getString(R.string.Benefit_Anulom5)+"\n\n"+getResources().getString(R.string.Benefit_Anulom6)+"\n\n"+getResources().getString(R.string.Benefit_Anulom7)+"\n\n"+getResources().getString(R.string.Benefit_Anulom8)+"\n\n"+getResources().getString(R.string.Benefit_Anulom9)+"\n\n"+getResources().getString(R.string.Benefit_Anulom10)+"\n\n"+getResources().getString(R.string.Benefit_Anulom11));
                heading_precaution.setText(R.string.Precautions);
                tv_content_pre.setText(getResources().getString(R.string.PRecaution_Anulom1)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom2)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom3)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom4));
                break;
        }

        linearLayout.addView(heading_benefit);
        linearLayout.addView(tv_content_benefit);
        linearLayout.addView(heading_precaution);
        linearLayout.addView(tv_content_pre);

        //return inflater.inflate(R.layout.fragment_benefits__setu_bandhasana, container, false);
        return mainLayout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
