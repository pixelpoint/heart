package com.habit.anulomvilom;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.habit.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Help_Pranayama.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Help_Pranayama#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Help_Pranayama extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int type;

    private OnFragmentInteractionListener mListener;

    public Help_Pranayama(int type) {
        this.type=type;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Help_Pranayama.
     */
    // TODO: Rename and change types and number of parameters
    public static Help_Pranayama newInstance(String param1, String param2,int type) {
        Help_Pranayama fragment = new Help_Pranayama(type);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup.LayoutParams lp     = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ViewGroup.LayoutParams linear = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        FrameLayout mainLayout        = new FrameLayout(getActivity());
        mainLayout.setLayoutParams(lp);
        mainLayout.setPadding(10,10,10,10);

        ScrollView scrollView     = new ScrollView(getActivity());
        scrollView.setLayoutParams(lp);

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(linear);
        linearLayout.setPadding(0,0,0,20);
        linearLayout.setBackgroundResource(R.drawable.stroke);

        mainLayout.addView(scrollView);
        scrollView.addView(linearLayout);

        TextView heading_main  = new TextView(getActivity());
        heading_main.setTextColor(Color.RED);
        heading_main.setTextSize(20);
        heading_main.setPadding(40,20,0,20);


        TextView heading_work  = new TextView(getActivity());
        heading_work.setTextColor(Color.RED);
        heading_work.setTextSize(20);
        heading_work.setPadding(40,20,0,20);

        TextView tv_content_pre = new TextView(getActivity());
        tv_content_pre.setPadding(60,10,20,0);
        tv_content_pre.setTextColor(Color.BLACK);

        switch (type){
            case 1:
                heading_main.setText(R.string.help_anulom1);
                heading_work.setText(R.string.help_anulom12);

                TextView tv_content1 = new TextView(getActivity());
                tv_content1.setPadding(60,10,20,0);
                tv_content1.setTextColor(Color.BLACK);
                tv_content1.setText(getResources().getString(R.string.help_anulom2)+"\n\n"+getResources().getString(R.string.help_anulom3));

                ImageView iv_hand    = new ImageView(getActivity());
                iv_hand.setImageResource(R.drawable.smile_good);

                TextView tv_content2 = new TextView(getActivity());
                tv_content2.setPadding(60,10,20,0);
                tv_content2.setTextColor(Color.BLACK);
                tv_content2.setText(getResources().getString(R.string.help_anulom4)+"\n\n"+getResources().getString(R.string.help_anulom5)+"\n\n"+getResources().getString(R.string.help_anulom6)+"\n\n"+getResources().getString(R.string.help_anulom7)+"\n\n"+getResources().getString(R.string.help_anulom8)+"\n\n"+getResources().getString(R.string.help_anulom9)+"\n\n"+getResources().getString(R.string.help_anulom10)+"\n\n"+getResources().getString(R.string.help_anulom11));

                TextView tv_content3 = new TextView(getActivity());
                tv_content3.setPadding(60,10,20,0);
                tv_content3.setTextColor(Color.BLACK);
                tv_content3.setText(getResources().getString(R.string.help_anulom13)+"\n\n"+getResources().getString(R.string.help_anulom14));

                ImageView iv_in_left    = new ImageView(getActivity());
                iv_in_left.setImageResource(R.drawable.smile_good);

                TextView tv_content4 = new TextView(getActivity());
                tv_content4.setPadding(60,10,20,0);
                tv_content4.setTextColor(Color.BLACK);
                tv_content4.setText(getResources().getString(R.string.help_anulom15));

                ImageView iv_hold   = new ImageView(getActivity());
                iv_hold.setImageResource(R.drawable.smile_good);

                TextView tv_content5 = new TextView(getActivity());
                tv_content5.setPadding(60,10,20,0);
                tv_content5.setTextColor(Color.BLACK);
                tv_content5.setText(getResources().getString(R.string.help_anulom16));

                ImageView iv_ex_right   = new ImageView(getActivity());
                iv_ex_right.setImageResource(R.drawable.smile_good);

                TextView tv_content6 = new TextView(getActivity());
                tv_content6.setPadding(60,10,20,0);
                tv_content6.setTextColor(Color.BLACK);
                tv_content6.setText(getResources().getString(R.string.help_anulom17));

                ImageView iv_hold1   = new ImageView(getActivity());
                iv_hold1.setImageResource(R.drawable.smile_good);

                TextView tv_content7 = new TextView(getActivity());
                tv_content7.setPadding(60,10,20,0);
                tv_content7.setTextColor(Color.BLACK);
                tv_content7.setText(getResources().getString(R.string.help_anulom18));

                ImageView iv_in_right   = new ImageView(getActivity());
                iv_in_right.setImageResource(R.drawable.smile_good);

                TextView tv_content8 = new TextView(getActivity());
                tv_content8.setPadding(60,10,20,0);
                tv_content8.setTextColor(Color.BLACK);
                tv_content8.setText(getResources().getString(R.string.help_anulom19));

                ImageView iv_hold2   = new ImageView(getActivity());
                iv_hold2.setImageResource(R.drawable.smile_good);

                TextView tv_content9 = new TextView(getActivity());
                tv_content9.setPadding(60,10,20,0);
                tv_content9.setTextColor(Color.BLACK);
                tv_content9.setText(getResources().getString(R.string.help_anulom20));

                ImageView iv_ex_left   = new ImageView(getActivity());
                iv_ex_left.setImageResource(R.drawable.smile_good);

                TextView tv_content10 = new TextView(getActivity());
                tv_content10.setPadding(60,10,20,0);
                tv_content10.setTextColor(Color.BLACK);
                tv_content10.setText(getResources().getString(R.string.help_anulom21));

                ImageView iv_hold3   = new ImageView(getActivity());
                iv_hold3.setImageResource(R.drawable.smile_good);

                TextView tv_content11 = new TextView(getActivity());
                tv_content11.setPadding(60,10,20,0);
                tv_content11.setTextColor(Color.BLACK);
                tv_content11.setText(getResources().getString(R.string.help_anulom22));

                linearLayout.addView(heading_main);
                linearLayout.addView(tv_content1);
                linearLayout.addView(iv_hand);
                linearLayout.addView(tv_content2);
                linearLayout.addView(heading_work);
                linearLayout.addView(tv_content3);
                linearLayout.addView(iv_in_left);
                linearLayout.addView(tv_content4);
                linearLayout.addView(iv_hold);
                linearLayout.addView(tv_content5);
                linearLayout.addView(iv_ex_right);
                linearLayout.addView(tv_content6);
                linearLayout.addView(iv_hold1);
                linearLayout.addView(tv_content7);
                linearLayout.addView(iv_in_right);
                linearLayout.addView(tv_content8);
                linearLayout.addView(iv_hold2);
                linearLayout.addView(tv_content9);
                linearLayout.addView(iv_ex_left);
                linearLayout.addView(tv_content10);
                linearLayout.addView(iv_hold3);
                linearLayout.addView(tv_content11);

                break;
            case 2:
                heading_main.setText(R.string.Help_Kapal1);
                heading_work.setText(R.string.Help_Kapal5);

                TextView tv_content_kapal1 = new TextView(getActivity());
                tv_content_kapal1.setPadding(60,10,20,0);
                tv_content_kapal1.setTextColor(Color.BLACK);
                tv_content_kapal1.setText(getResources().getString(R.string.Help_Kapal2));

                ImageView iv_position    = new ImageView(getActivity());
                iv_position.setImageResource(R.drawable.smile_good);

                TextView tv_content_kapal2 = new TextView(getActivity());
                tv_content_kapal2.setPadding(60,10,20,0);
                tv_content_kapal2.setTextColor(Color.BLACK);
                tv_content_kapal2.setText(getResources().getString(R.string.Help_Kapal3)+"\n\n"+getResources().getString(R.string.Help_Kapal4));

                TextView tv_content_kapal3 = new TextView(getActivity());
                tv_content_kapal3.setPadding(60,10,20,0);
                tv_content_kapal3.setTextColor(Color.BLACK);
                tv_content_kapal3.setText(getResources().getString(R.string.Help_Kapal6)+"\n\n"+getResources().getString(R.string.Help_Kapal7));

                ImageView iv_out    = new ImageView(getActivity());
                iv_out.setImageResource(R.drawable.smile_good);

                TextView tv_content_kapal4 = new TextView(getActivity());
                tv_content_kapal4.setPadding(60,10,20,0);
                tv_content_kapal4.setTextColor(Color.BLACK);
                tv_content_kapal4.setText(getResources().getString(R.string.Help_Kapal8));

                ImageView iv_in   = new ImageView(getActivity());
                iv_in.setImageResource(R.drawable.smile_good);

                TextView tv_content_kapal5 = new TextView(getActivity());
                tv_content_kapal5.setPadding(60,10,20,0);
                tv_content_kapal5.setTextColor(Color.BLACK);
                tv_content_kapal5.setText(getResources().getString(R.string.Help_Kapal9));


                linearLayout.addView(heading_main);
                linearLayout.addView(tv_content_kapal1);
                linearLayout.addView(iv_position);
                linearLayout.addView(tv_content_kapal2);
                linearLayout.addView(heading_work);
                linearLayout.addView(tv_content_kapal3);
                linearLayout.addView(iv_out);
                linearLayout.addView(tv_content_kapal4);
                linearLayout.addView(iv_in);
                linearLayout.addView(tv_content_kapal5);

                break;
            case 3:
                heading_main.setText(R.string.Help_Bhramari1);
                heading_work.setText(R.string.Help_Bhramari10);

                TextView tv_content_bhr1 = new TextView(getActivity());
                tv_content_bhr1.setPadding(60,10,20,0);
                tv_content_bhr1.setTextColor(Color.BLACK);
                tv_content_bhr1.setText(getResources().getString(R.string.Help_Bhramari2)+"\n\n"+getResources().getString(R.string.Help_Bhramari3)+"\n\n"+getResources().getString(R.string.Help_Bhramari4)+"\n\n"+getResources().getString(R.string.Help_Bhramari5));

                ImageView iv_position_bhr    = new ImageView(getActivity());
                iv_position_bhr.setImageResource(R.drawable.smile_good);

                TextView tv_content_bhr2 = new TextView(getActivity());
                tv_content_bhr2.setPadding(60,10,20,0);
                tv_content_bhr2.setTextColor(Color.BLACK);
                tv_content_bhr2.setText(getResources().getString(R.string.Help_Bhramari5)+"\n\n"+getResources().getString(R.string.Help_Bhramari6)+"\n\n"+getResources().getString(R.string.Help_Bhramari7)+"\n\n"+getResources().getString(R.string.Help_Bhramari8)+"\n\n"+getResources().getString(R.string.Help_Bhramari9));


                TextView tv_content_bhr3 = new TextView(getActivity());
                tv_content_bhr3.setPadding(60,10,20,0);
                tv_content_bhr3.setTextColor(Color.BLACK);
                tv_content_bhr3.setText(getResources().getString(R.string.Help_Bhramari11)+"\n\n"+getResources().getString(R.string.Help_Bhramari12));

                ImageView iv_in_both    = new ImageView(getActivity());
                iv_in_both.setImageResource(R.drawable.smile_good);

                TextView tv_content_bhr4 = new TextView(getActivity());
                tv_content_bhr4.setPadding(60,10,20,0);
                tv_content_bhr4.setTextColor(Color.BLACK);
                tv_content_bhr4.setText(getResources().getString(R.string.Help_Bhramari13));

                ImageView iv_out_both   = new ImageView(getActivity());
                iv_out_both.setImageResource(R.drawable.smile_good);

                TextView tv_content_bhr5 = new TextView(getActivity());
                tv_content_bhr5.setPadding(60,10,20,0);
                tv_content_bhr5.setTextColor(Color.BLACK);
                tv_content_bhr5.setText(getResources().getString(R.string.Help_Bhramari14));

                linearLayout.addView(heading_main);
                linearLayout.addView(tv_content_bhr1);
                linearLayout.addView(iv_position_bhr);
                linearLayout.addView(tv_content_bhr2);
                linearLayout.addView(heading_work);
                linearLayout.addView(tv_content_bhr3);
                linearLayout.addView(iv_in_both);
                linearLayout.addView(tv_content_bhr4);
                linearLayout.addView(iv_out_both);
                linearLayout.addView(tv_content_bhr5);

                break;
            /*default:
                heading_main.setText(R.string.Benefit_Anulom);
              //  tv_content_benefit.setText(getResources().getString(R.string.Benefit_Anulom1)+"\n\n"+getResources().getString(R.string.Benefit_Anulom2)+"\n\n"+getResources().getString(R.string.Benefit_Anulom3)+"\n\n"+getResources().getString(R.string.Benefit_Anulom4)+"\n\n"+getResources().getString(R.string.Benefit_Anulom5)+"\n\n"+getResources().getString(R.string.Benefit_Anulom6)+"\n\n"+getResources().getString(R.string.Benefit_Anulom7)+"\n\n"+getResources().getString(R.string.Benefit_Anulom8)+"\n\n"+getResources().getString(R.string.Benefit_Anulom9)+"\n\n"+getResources().getString(R.string.Benefit_Anulom10)+"\n\n"+getResources().getString(R.string.Benefit_Anulom11));
                heading_work.setText(R.string.Precautions);
                tv_content_pre.setText(getResources().getString(R.string.PRecaution_Anulom1)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom2)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom3)+"\n\n"+getResources().getString(R.string.PRecaution_Anulom4));
                break;*/
        }


//        linearLayout.addView(heading_work);
       // linearLayout.addView(tv_content_pre);

        //return inflater.inflate(R.layout.fragment_benefits__setu_bandhasana, container, false);
        return mainLayout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
