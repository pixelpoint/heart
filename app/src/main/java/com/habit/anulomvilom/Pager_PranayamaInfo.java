package com.habit.anulomvilom;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by india on 27-Jul-16.
 */
public class Pager_PranayamaInfo extends FragmentStatePagerAdapter {

    int tabCount;
    int type;

    public Pager_PranayamaInfo(FragmentManager fm, int tabCount,int type) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.type=type;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                About_Pranayama steps= new About_Pranayama(type);
                return steps;
            case 1:
                Benefits_Pranayama benefits = new Benefits_Pranayama(type);
                return benefits;
            case 2:
                Help_Pranayama precautions = new Help_Pranayama(type);
                return precautions;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
