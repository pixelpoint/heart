package com.habit.bhramari;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.habit.MainReportActivity;
import com.habit.MyNumberPicker;
import com.habit.R;
import com.habit.Sounds;
import com.habit.anulomvilom.ActivityPranayamaInfo;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;
import com.habit.kapalbhati.Kapalbhati_Activity;
import com.habit.report.ReportActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BhramariPranayama_Activity extends AppCompatActivity {
    ImageView im_data, im_back1, im_forword1, im_inhel, im_pause, im_backbutton, tv_start, tv_stop, iv_reset,iv_help;
    TextView tv_inhalechange, tv_inhaltimer, tv_timerhr, tv_timermin, tv_timersec, tv_timerhr1, tv_timermin1, tv_timersec1,tv_exhale_h,tv_inhale_h,tv_round_h,tv_totaltime_h,tv_action_h,tv_bhramari_h;
    LinearLayout ll_start, ll_stop,ll_total,content_bhramari;
    MediaPlayer mp;
    RelativeLayout rl_instruction;
    Button bt_instruction;
    MyNumberPicker np_inhale,np_exhale,np_rounds;
    Context con;
//Values of Inhale,Hold and Exhale
    int i = 1,e = 8,r = 1;
    int m=0;
//Set Timer
    int hr, min, sec, tempsec, tt;
    String time;
    int tempI,tempE = 3;
//using flag for pause the thread and maintain the values
    int flag = 0;
//p for preparation time ans se for reduce the tempsecond when preparation time become zero
    int p,se=0;
    int tempP=p;
//we use a long sound clip then sm is set when the sound is play and it set to zero when not play then we pause the sound on pause on back pressed on stop when sm=1
    int sm=0;
//set the launches of bhramari
    int valuechangebhramari,btnbhramari;
    Thread t;
    Boolean sound, sec_sound, vibration;
    Vibrator v;
    int totaltime;
    Sounds sounds;
    int habit_start=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_bhramari_pranayama_);

        con                 = this;
        im_data             = (ImageView) findViewById(R.id.im_data);
        im_back1            = (ImageView) findViewById(R.id.im_back1);
        im_forword1         = (ImageView) findViewById(R.id.im_forward1);
        im_inhel            = (ImageView) findViewById(R.id.im_inhale);
        im_pause            = (ImageView) findViewById(R.id.im_pause);
        iv_help             = (ImageView) findViewById(R.id.iv_help);
        im_backbutton       = (ImageView) findViewById(R.id.im_backbutton);
        tv_timerhr          = (TextView) findViewById(R.id.tv_timerhr);
        tv_timermin         = (TextView) findViewById(R.id.tv_timermin);
        tv_timersec         = (TextView) findViewById(R.id.tv_timersec);
        tv_timerhr1         = (TextView) findViewById(R.id.tv_timerhr1);
        tv_timermin1        = (TextView) findViewById(R.id.tv_timermin1);
        tv_timersec1        = (TextView) findViewById(R.id.tv_timersec1);
        tv_inhalechange     = (TextView) findViewById(R.id.tv_inhalechange);
        tv_inhaltimer       = (TextView) findViewById(R.id.tv_inhaletimer);

        tv_inhale_h         = (TextView) findViewById(R.id.tv_inhale_h);
        tv_exhale_h         = (TextView) findViewById(R.id.tv_exhale_h);
        tv_round_h          = (TextView) findViewById(R.id.tv_round_h);
        tv_totaltime_h      = (TextView) findViewById(R.id.tv_totaltime_h);
        tv_action_h         = (TextView) findViewById(R.id.tv_action_h);
        tv_bhramari_h       = (TextView) findViewById(R.id.tv_bhramari_h);

        tv_start            = (ImageView) findViewById(R.id.tv_start);
        tv_stop             = (ImageView) findViewById(R.id.tv_stop);
        ll_start            = (LinearLayout) findViewById(R.id.ll_start);
        ll_stop             = (LinearLayout) findViewById(R.id.ll_stop);
        iv_reset            = (ImageView) findViewById(R.id.iv_reset);
        ll_total            = (LinearLayout) findViewById(R.id.ll_total);

        np_inhale           = (MyNumberPicker) findViewById(R.id.np_inhale);
        np_exhale           = (MyNumberPicker) findViewById(R.id.np_exhale);
        np_rounds           = (MyNumberPicker) findViewById(R.id.np_rounds);

        content_bhramari    = (LinearLayout) findViewById(R.id.content_bhramari);

        sounds              = new Sounds(con);
        habit_start         = GlobalClass.callSavedPreferences2("habit_start",habit_start,con);
        p                   = GlobalClass.callSavedPreferences2("preparation", p, con);

        v                   = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//call the language state from setting activity
        // changeLang();
        numberPick();

//For change the value of Inhale,Hold and Exhale
        im_forword1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i < 20) {
                    i = i + 1;
                    e = i * 2;
                    setStartTime();
                    GlobalClass.savePreferences2("inhalebhar", i, con);
                    GlobalClass.savePreferences2("exhalebhar", e, con);
                    GlobalClass.savePreferences2("roundbhar", r, con);
                    GlobalClass.savePreferences2("btnbhramari", 2, con);
                    numberPick();
                } else if (i == 20) {
                    e = i * 2;
                    setStartTime();
                    GlobalClass.savePreferences2("inhalebhar", i, con);
                    GlobalClass.savePreferences2("exhalebhar", e, con);
                    GlobalClass.savePreferences2("roundbhar", r, con);
                    GlobalClass.savePreferences2("btnbhramari", 2, con);
                    numberPick();
                }
            }
        });

//Reduced the value of inhale,hold and exhale simultaneously
        im_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i > 1) {
                    i = i - 1;
                    e = i * 2;
                    setStartTime();
                    GlobalClass.savePreferences2("inhalebhar", i, con);
                    GlobalClass.savePreferences2("exhalebhar", e, con);
                    GlobalClass.savePreferences2("roundbhar", r, con);
                    GlobalClass.savePreferences2("btnbhramari", 2, con);
                    numberPick();
                } else if (i == 1) {
                    e = i * 2;
                    setStartTime();
                    GlobalClass.savePreferences2("inhalebhar", i, con);
                    GlobalClass.savePreferences2("exhalebhar", e, con);
                    GlobalClass.savePreferences2("roundbhar", r, con);
                    GlobalClass.savePreferences2("btnbhramari", 2, con);
                    numberPick();
                }
            }
        });

//reset button for reset the value of inhale exhale and rounds by 5 : 10 ,1
        iv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetValue();

            }
        });

//Start Button Functionality
        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });

//Stop Button
        tv_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                if (sm == 1) {
                    mp.pause();
                    sm = 0;
                }
                restart();
                flag = 0;
            }
        });
//Pause Button
        im_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                if (sm == 1) {
                    mp.pause();
                    sm = 0;
                }
                tv_stop.setVisibility(View.GONE);
                tv_start.setVisibility(View.VISIBLE);
                flag = 1;
            }
        });
//for database
        im_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(con, ReportActivity.class);
                in.putExtra("type",3);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
//help
        im_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ll_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(con,ActivityPranayamaInfo.class);
                intent.putExtra("type",3);
                startActivity(intent);
            }
        });

    }

    public void play(){

        //Call savedPreference of setting component
        sound = GlobalClass.callSavedPreferences1("sound", con);
        sec_sound = GlobalClass.callSavedPreferences1("second_sound", con);
        vibration = GlobalClass.callSavedPreferences1("vibration", con);
        setDisabled();
        m=1;
        sm=0;
        if (flag == 0) {
            tempI = i;
            tempP=p;
            tt = tempsec;
        }
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCountDownTime();
                                if (tempsec > 0) {

                                    if(tempP <=p && tempP>0){
                                        sounds.playSound1();
                                        tv_inhalechange.setText(R.string.Prepare);
                                        tv_inhaltimer.setText(String.valueOf(tempP));

                                        tempP = tempP - 1;
                                        if (tempP == 0)
                                            tempI= i;
                                    }

                                    else if (tempI <= i && tempI > 0) {
                                        if (tempI == i)
                                            sounds.inhale_sound();
                                        else
                                            sounds.playSound1();
                                        v.vibrate(1000);

                                        tv_inhalechange.setText(R.string.Inhale_both);
                                        tv_inhaltimer.setText(String.valueOf(tempI));
                                        im_inhel.setImageResource(R.drawable.action_inhale_both);


                                        tempI = tempI - 1;
                                        se=1;
                                        if (tempI == 0)
                                            tempE = e;


                                    } else if (tempE <= e && tempE > 0) {
                                        if (tempE == e)
                                            sounds.exhale_sound();
                                        else{
                                            playSound4();
                                            sm=1;
                                        }

                                        tv_inhalechange.setText(R.string.Exhale_both);
                                        tv_inhaltimer.setText(String.valueOf(tempE));
                                        im_inhel.setImageResource(R.drawable.exhale_bhramari);

                                        tempE = tempE - 1;
                                        se=1;

                                        if (tempE == 0) {
                                            tempI = i;
                                            // mp.pause();
                                            sm=0;
                                        }
                                    }

                                }// outer if close
                                else {
                                    sounds.playSound3();
                                    t.interrupt();
                                    restart();
                                }
                                if(se==1) {
                                    tempsec = tempsec - 1;
                                    se=0;
                                }
                            }  // run close

                        }); // Runnable close

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }
    //for set the time
    public void setStartTime() {
        tempsec = ((i  + e)  * r);
        hr = tempsec / 3600;
        int v = tempsec % 3600;
        min = v / 60;
        sec = v % 60;
        time = String.format("  %02d     %02d     %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

    //Change the time
    public void setCountDownTime() {
        hr = tempsec / 3600;
        int v = tempsec % 3600;
        min = v / 60;
        sec = v % 60;
        time = String.format("  %02d     %02d     %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

    //Reset the all contents when complete or stop
    public void restart() {
        im_inhel.setImageBitmap(null);
        tv_inhalechange.setText("");
        tv_inhaltimer.setText("");
        tv_start.setVisibility(View.VISIBLE);
        tv_stop.setVisibility(View.GONE);
        im_pause.setVisibility(View.INVISIBLE);
        ll_start.setVisibility(View.GONE);
        ll_stop.setVisibility(View.VISIBLE);

        setEnabled();

        int doTime = tt - tempsec;

        totaltime = doTime+totaltime;

        setStartTime();
        if(doTime!=0) {
            DatabaseHandler db = new DatabaseHandler(con);
            //db.addContact(i,0,e, r, 3, tt, doTime,str_date,str_datesep,"null");
            db.addPranayamaTimes(String.format("%2d:%2d", i, e), r, 3, tt, doTime);
        }
        m=0;
        tempP=p;
        sm=0;
        if (habit_start==1){
            GlobalClass.savePreferences2("habit_start",0,con);
            startActivity(new Intent(con, MainReportActivity.class));
        }
    }

    public void playSound4() {
        try {
            mp = MediaPlayer.create(getBaseContext(), (R.raw.bss_3));
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (sound && mp != null)
                        mp.start();
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    //Method for change the colour and disable the arrow of inhale and rounds
    public void setDisabled() {
        tv_start.setVisibility(View.GONE);
        tv_stop.setVisibility(View.VISIBLE);
        ll_stop.setVisibility(View.GONE);
        ll_start.setVisibility(View.VISIBLE);

        iv_reset.setVisibility(View.INVISIBLE);
        im_data.setVisibility(View.INVISIBLE);
        im_backbutton.setVisibility(View.INVISIBLE);
        im_pause.setVisibility(View.VISIBLE);
        iv_help.setVisibility(View.GONE);
    }

//Method for change the colour and enable the arrow of inhale and rounds
    public void setEnabled() {
        im_backbutton.setVisibility(View.VISIBLE);
        iv_reset.setVisibility(View.VISIBLE);
        im_data.setVisibility(View.VISIBLE);
        im_pause.setVisibility(View.GONE);
        iv_help.setVisibility(View.VISIBLE);
    }

    public void changeLang() {
        String str_lang = "";
        switch (GlobalClass.callSavedPreferences2("language", 0, con)){
            case 0:
                str_lang="en";
                break;
            case 1:
                str_lang="hi";
                break;
            case 2:
                str_lang="ru";
                break;
            case 3:
                str_lang="fr";
                break;
            case 4:
                str_lang="de";
                break;
            case 5:
                str_lang="es";
                break;
            default:
                str_lang="en";
                break;
        }
        Locale myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_inhale_h.setText(R.string.Inhale);
        tv_exhale_h.setText(R.string.Exhale);
        tv_round_h.setText(R.string.Rounds);
        tv_totaltime_h.setText(R.string.Total_Time);
        tv_action_h.setText(R.string.Action);
        tv_bhramari_h.setText(R.string.Bhramari);
    }


    public void resetValue(){
        i = 5;
        e = 10;
        r = 4;
        setStartTime();
        GlobalClass.savePreferences2("inhalebhar", i, con);
        GlobalClass.savePreferences2("exhalebhar", e, con);
        GlobalClass.savePreferences2("roundbhar", r, con);
        numberPick();
    }

    public void numberPick(){
//make divider transparent
        setDividerColor(np_inhale, Color.TRANSPARENT);
        setDividerColor(np_exhale, Color.TRANSPARENT);
        setDividerColor(np_rounds, Color.TRANSPARENT);
        valuechangebhramari=GlobalClass.callSavedPreferences2("valuechangebhramari",valuechangebhramari,con);
        btnbhramari=GlobalClass.callSavedPreferences2("btnbhramari",btnbhramari,con);


        if(valuechangebhramari==2 || btnbhramari == 2) {
            i = GlobalClass.callSavedPreferences2("inhalebhar", i, con);
            e = GlobalClass.callSavedPreferences2("exhalebhar", e, con);
            r = GlobalClass.callSavedPreferences2("roundbhar", r, con);
        }
        else{
            i=5;
            e=10;
            r=4;
            GlobalClass.savePreferences2("inhalebhar", i, con);
            GlobalClass.savePreferences2("exhalebhar", e, con);
            GlobalClass.savePreferences2("roundbhar", r, con);
        }
//set minimum and maximum values
        np_inhale.setMinValue(1);
        np_inhale.setMaxValue(20);
        np_exhale.setMinValue(1);
        np_exhale.setMaxValue(40);
        np_rounds.setMinValue(1);
        np_rounds.setMaxValue(50);

        np_inhale.setValue(i);
        np_exhale.setValue(e);
        np_rounds.setValue(r);

        setStartTime();

        np_inhale.setWrapSelectorWheel(true);
        np_inhale.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=newVal;
                e=np_exhale.getValue();
                r=np_rounds.getValue();
                GlobalClass.savePreferences2("inhalebhar", i, con);
                GlobalClass.savePreferences2("exhalebhar", e, con);
                GlobalClass.savePreferences2("roundbhar", r, con);
                GlobalClass.savePreferences2("valuechangebhramari",2,con);
                setStartTime();
            }
        });


        np_exhale.setWrapSelectorWheel(true);
        np_exhale.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                e=newVal;
                r=np_rounds.getValue();
                GlobalClass.savePreferences2("inhalebhar", i, con);
                GlobalClass.savePreferences2("exhalebhar", e, con);
                GlobalClass.savePreferences2("roundbhar", r, con);
                GlobalClass.savePreferences2("valuechangebhramari",2,con);
                setStartTime();
            }
        });

        np_rounds.setWrapSelectorWheel(true);
        np_rounds.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                i=np_inhale.getValue();
                e=np_exhale.getValue();
                r=newVal;
                GlobalClass.savePreferences2("inhalebhar", i, con);
                GlobalClass.savePreferences2("exhalebhar", e, con);
                GlobalClass.savePreferences2("roundbhar", r, con);
                GlobalClass.savePreferences2("valuechangebhramari",2,con);
                setStartTime();
            }
        });
    }

    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onResume(){
        super.onResume();
        sounds     = new Sounds(con);
        changeLang();
        if (GlobalClass.callSavedPreferences1("display", con))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onBackPressed() {
        if(m==1)
            t.interrupt();
        if(sm==1){
            mp.pause();
            sm=0;
        }
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        GlobalClass.savePreferences2("habit_start",0,con);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}