package com.habit.charting.interfaces.dataprovider;

import com.habit.charting.components.YAxis.AxisDependency;
import com.habit.charting.data.BarLineScatterCandleBubbleData;
import com.habit.charting.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(AxisDependency axis);
    boolean isInverted(AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
