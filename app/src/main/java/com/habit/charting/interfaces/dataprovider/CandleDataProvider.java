package com.habit.charting.interfaces.dataprovider;

import com.habit.charting.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    CandleData getCandleData();
}
