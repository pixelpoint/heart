package com.habit.charting.interfaces.dataprovider;

import com.habit.charting.components.YAxis;
import com.habit.charting.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
