package com.habit.charting.interfaces.dataprovider;

import com.habit.charting.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
