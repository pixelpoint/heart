package com.habit.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DatabaseHandler extends SQLiteOpenHelper {
    private ReportModel reportModel;

    private static final int DATABASE_VERSION       = 1;
	  
    private static final String DATABASE_NAME       = "Heart.db";

    private static final String TABLE_PRANAYAMA     = "table_pranayama";
    private static final String KEY_ID              = "id";
    private static final String KEY_LEVEL           = "level";
    private static final String KEY_ROUNDS          = "rounds";
    private static final String KEY_TOTAL_DURATION  = "time";
    private static final String KEY_DO_DURATION     = "duration";
    private static final String KEY_TYPE            = "type";
    private static final String KEY_TIMESTAMP       = "timestamp";

    private static final String TABLE_YOGA          = "table_yoga";
    private static final String KEY_ID_YOGA         = "id_yoga";
    private static final String KEY_TIME_YOGA       = "time_yoga";
    private static final String KEY_TYPE_YOGA       = "type_yoga";
    private static final String KEY_STTS_YOGA       = "stts_yoga";
    private static final String KEY_TIMESTAMP_YOGA  = "timestamp_yoga";

    private static final String TABLE_STEPS         = "table_steps";
    private static final String KEY_ID_STEPS        = "id_steps";
    private static final String KEY_COUNT_STEPS     = "count_steps";
    private static final String KEY_MAX_STEPS       = "max_steps";
    private static final String KEY_TOTAL_TIME      = "total_time";
    private static final String KEY_TIMESTAMP_STEPS = "timestamp_steps";

 	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

        String CREATE_PRANAYAMA_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_PRANAYAMA+ "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_LEVEL + " TEXT,"
                + KEY_ROUNDS + " TEXT," + KEY_TOTAL_DURATION + " TEXT,"
                + KEY_DO_DURATION + " INTEGER," + KEY_TYPE + " INTEGER," +  KEY_TIMESTAMP + " DEFAULT CURRENT_TIMESTAMP" + ")";

        String CREATE_YOGA_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_YOGA+ "("
                + KEY_ID_YOGA + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TIME_YOGA + " INTEGER,"
                + KEY_TYPE_YOGA + " INTEGER," + KEY_STTS_YOGA + " INTEGER," +  KEY_TIMESTAMP_YOGA + " DEFAULT CURRENT_TIMESTAMP" + ")";

        String CREATE_STEPS_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_STEPS+ "("
                + KEY_ID_STEPS + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_COUNT_STEPS + " INTEGER,"+ KEY_MAX_STEPS + " INTEGER,"
                + KEY_TOTAL_TIME + " INTEGER,"+ KEY_TIMESTAMP_STEPS + " DEFAULT CURRENT_TIMESTAMP" +")";

        db.execSQL(CREATE_PRANAYAMA_TABLE);
        db.execSQL(CREATE_YOGA_TABLE);
        db.execSQL(CREATE_STEPS_TABLE);
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DatabaseHandler.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        if (newVersion>oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRANAYAMA);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_YOGA);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STEPS);
        }
        onCreate(db);

    }  // method body finish

    public void addSteps(int steps,int max_steps,int total_time) {
        createTables();
        SQLiteDatabase db    = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_COUNT_STEPS,steps);
        values.put(KEY_MAX_STEPS,max_steps);
        values.put(KEY_TOTAL_TIME,total_time);

        System.out.println("================================================================");
        System.out.println(steps);
        System.out.println(max_steps);
        System.out.println(total_time);
        System.out.println("================================================================");

        db.insert(TABLE_STEPS, null, values);
        db.close(); // Closing database connection
    }
    public ArrayList<ReportModel> getSteps(String current_date){
        createTables();
        String selectQuery="";
        ArrayList<ReportModel> mlist  =new ArrayList<ReportModel>();
        if (current_date.equals("0"))
            selectQuery = "SELECT SUM(count_steps), SUM(total_time), max_steps, date(timestamp_steps, 'localtime') FROM " + TABLE_STEPS+" GROUP BY date(timestamp_steps, 'localtime')";
        else
            selectQuery = "SELECT SUM(count_steps), SUM(total_time), max_steps, date(timestamp_steps, 'localtime') FROM " + TABLE_STEPS+" where date(timestamp_steps, 'localtime') = '" + current_date + "' GROUP BY date(timestamp_steps, 'localtime')";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                reportModel =new ReportModel();
                reportModel.setTotal_steps(cursor.getInt(0));
                reportModel.setTotal_time(cursor.getInt(1));
                reportModel.setMax_steps(cursor.getInt(2));
                reportModel.setDate(cursor.getString(3));
                mlist.add(reportModel);

            } while (cursor.moveToNext());
        }// if complete

        db.close();
        return mlist;
    }

    public void addPranayamaTimes(String level, int rounds, int type, int totaltime, int duration) {
        createTables();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        /*SimpleDateFormat dateFormatsep= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String str_datesep=dateFormatsep.format(new Date());*/

        values.put(KEY_LEVEL,level);
        values.put(KEY_ROUNDS,rounds);
        values.put(KEY_TOTAL_DURATION,totaltime);
        values.put(KEY_DO_DURATION, duration);
        values.put(KEY_TYPE, type);

        db.insert(TABLE_PRANAYAMA, null, values);
        db.close(); // Closing database connection
    } // addcontact body complete


    public ArrayList<ReportModel>  getPranayamaTimes(int type,String current_date,String end_date) {
        createTables();
        String selectQuery="";
        ArrayList<ReportModel> mlist =  new ArrayList<ReportModel>();
        if (current_date.equals("0"))
            selectQuery=  "SELECT id, level, rounds, time, duration, datetime(timestamp, 'localtime') FROM " + TABLE_PRANAYAMA + " where type = '" + type + "'";
        else
            selectQuery=  "SELECT id, level, rounds, time, sum(duration), datetime(timestamp, 'localtime') FROM " + TABLE_PRANAYAMA + " where type = '" + type + "' AND date(timestamp, 'localtime') >= '"+current_date+"' AND date(timestamp, 'localtime') <= '" + end_date +"'";
        //String selectQuery="SELECT * FROM " + eTABLE + " where type = '" + type + "' AND " + KEY_DATE + " BETWEEN '" + startd + "' AND '" + endd + "'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int serialNo=cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                reportModel =new ReportModel();
                reportModel.setSno(String.valueOf(serialNo));
                reportModel.setId(cursor.getString(0));
                reportModel.setLevel(cursor.getString(1));
                reportModel.setRounds(cursor.getString(2));
                reportModel.setTotal_time(cursor.getInt(3));
                reportModel.setDo_time(cursor.getInt(4));
                reportModel.setDatesep(cursor.getString(5));

                mlist.add(reportModel);
                serialNo--;

            } while (cursor.moveToNext());
        }// if complete

        db.close();
        return mlist;

    }// method body finish

    public ArrayList<ReportModel>  getDoTime(int type) {
        createTables();
        String selectQuery="";
        ArrayList<ReportModel> mlist =  new ArrayList<ReportModel>();

        selectQuery=  "SELECT sum(duration) FROM " + TABLE_PRANAYAMA + " where type = '" + type + "'";
        //String selectQuery="SELECT * FROM " + eTABLE + " where type = '" + type + "' AND " + KEY_DATE + " BETWEEN '" + startd + "' AND '" + endd + "'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                reportModel =new ReportModel();
                reportModel.setDuration(getTime(cursor.getInt(0)));
                mlist.add(reportModel);

            } while (cursor.moveToNext());
        }

        db.close();
        return mlist;

    }// method body finish

    public ArrayList<ReportModel>  getAllData() {

        createTables();

        ArrayList<ReportModel> mlist  =new ArrayList<ReportModel>();
        String selectQuery = "SELECT sum(duration), date(timestamp, 'localtime'),type FROM " + TABLE_PRANAYAMA+" GROUP BY date(timestamp, 'localtime'),type";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                reportModel =new ReportModel();
                String duration=getTime(cursor.getInt(0));
                reportModel.setDuration(duration);
                reportModel.setDate(cursor.getString(1));
                reportModel.setType1(cursor.getInt(2));
                mlist.add(reportModel);

            } while (cursor.moveToNext());
        }// if complete

        db.close();
        return mlist;
    }// method body finish

    public void addYoga(int type, int time, int stts) {
        createTables();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_TYPE_YOGA,type);
        values.put(KEY_TIME_YOGA,time);
        values.put(KEY_STTS_YOGA,stts);

        db.insert(TABLE_YOGA, null, values);
        db.close(); // Closing database connection
    } // addcontact body complete

    public ArrayList<ReportModel>  getYoga(int type,String current_date) {
        createTables();

        ArrayList<ReportModel> mlist =  new ArrayList<ReportModel>();
        String selectQuery=  "SELECT * FROM " + TABLE_YOGA+ " where type_yoga = '" + type + "' AND date(timestamp_yoga, 'localtime') = '" + current_date + "' GROUP BY date(timestamp_yoga, 'localtime')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int serialNo=cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                reportModel =new ReportModel();
                reportModel.setSno(String.valueOf(serialNo));
                reportModel.setId(cursor.getString(0));
                reportModel.setDo_time(cursor.getInt(1));
                reportModel.setType1(cursor.getInt(2));
                reportModel.setStts_yoga(cursor.getInt(3));
                reportModel.setDate(cursor.getString(4));

                mlist.add(reportModel);
                serialNo--;

            } while (cursor.moveToNext());
        }// if complete

        db.close();
        return mlist;

    }// method body finish


    public void deleteContact(String no) {
            SQLiteDatabase db = this.getWritableDatabase();
	        db.delete(TABLE_PRANAYAMA, KEY_ID + " = ?",
                    new String[]{no});
	        db.close();
	    }  //method body finish

    public void deleteAll(String date,int type) {
        createTables();
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRANAYAMA, "date(timestamp, 'localtime')" + " = ? AND " + KEY_TYPE + " = ?",
                new String[] { date,type+"" });
        db.close();

    }  //method body finish


    /*public void deleteYogasana(String name,int habit_id) {

        createTables();

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_ASANAS,
                    KEY_NAME_ASANAS + " = ? AND " + KEY_ID_CUSTOM_HABIT + " = ?",
                    new String[] {name, habit_id+""});
        db.close();
    }  //method body finish*/

    public void createTables(){
        SQLiteDatabase db = this.getWritableDatabase();

        String CREATE_PRANAYAMA_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_PRANAYAMA+ "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_LEVEL + " TEXT,"
                + KEY_ROUNDS + " TEXT," + KEY_TOTAL_DURATION + " TEXT,"
                + KEY_DO_DURATION + " INTEGER," + KEY_TYPE + " INTEGER," +  KEY_TIMESTAMP + " DEFAULT CURRENT_TIMESTAMP" + ")";

        String CREATE_YOGA_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_YOGA+ "("
                + KEY_ID_YOGA + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TIME_YOGA + " INTEGER,"
                + KEY_TYPE_YOGA + " INTEGER," + KEY_STTS_YOGA + " INTEGER," +  KEY_TIMESTAMP_YOGA + " DEFAULT CURRENT_TIMESTAMP" + ")";

        String CREATE_STEPS_TABLE = "CREATE TABLE IF NOT EXISTS " +TABLE_STEPS+ "("
                + KEY_ID_STEPS + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_COUNT_STEPS + " INTEGER,"+ KEY_MAX_STEPS + " INTEGER,"
                + KEY_TOTAL_TIME + " INTEGER,"+ KEY_TIMESTAMP_STEPS + " DEFAULT CURRENT_TIMESTAMP" +")";

        db.execSQL(CREATE_PRANAYAMA_TABLE);
        db.execSQL(CREATE_YOGA_TABLE);
        db.execSQL(CREATE_STEPS_TABLE);
        db.close();
    }

    public String getTime(int duration) {

        int hr = duration / 3600;
        int v = duration % 3600;
        int min = v / 60;
        int sec = v % 60;

        String time="";
        if(hr!=0){

                    if(min!=0)
                    {
                              if(sec!=0)
                                  time =""+hr+"hour"+" "+min+"min"+" "+sec+"sec";
                              else
                                  time =""+hr+"hour"+" "+min+"min";
                    }
                    else{
                            if(sec!=0)
                                time =""+hr+"hour"+" "+sec+"sec";
                            else
                                time =""+hr+"hour";
                     }
        }
        else{
                    if(min!=0)
                    {
                            if(sec!=0)
                                time = ""+min+"min"+" "+sec+"sec";
                            else
                                time = ""+min+"min";
                    }
                    else{
                        time = ""+sec+"sec";
                    }
        }

         return time;
    }
}
