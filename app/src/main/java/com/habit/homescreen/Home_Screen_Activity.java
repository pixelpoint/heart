package com.habit.homescreen;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.habit.MainReportActivity;
import com.habit.NotificationReceiver;
import com.habit.R;
import com.habit.ScrollChangeListner;
import com.habit.SettingActivity;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;
import com.habit.floatingbutton.FloatingActionButton;
import com.habit.floatingbutton.FloatingActionsMenu;
import com.habit.stepcounter.Step_Counter_Activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Home_Screen_Activity extends AppCompatActivity {

    Context context;
    LinearLayout ll_steps;
    ReportModel reportModel;
    TextView tv_last_steps, tv_heading, tv_yoga, tv_pranayama, tv_step_count;
    CardView card_steps;
    ArrayList<ReportModel> yoga_list;
    ArrayList<ReportModel> pranayama_list;
    ArrayList<ReportModel> last_steps;
    ArrayList<ReportModel> list_time;
    ArrayList<String> time;
    Recycler_adapter adapter_yoga, adapter_pranayama;
    RecyclerView list_yoga, list_pranayama;
    DatabaseHandler db;
    FloatingActionsMenu floatingMenu;
    FloatingActionButton action_setting, action_report, action_play;
    ScrollChangeListner sv_main;
    String[] yoga, pranayama,alternate_yoga,alternate_pranayama;
    Integer[] img_yoga,img_prana;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home__screen_);

        context             = this;
        ll_steps            = (LinearLayout) findViewById(R.id.ll_steps);
        card_steps          = (CardView) findViewById(R.id.card_view);
        list_yoga           = (RecyclerView) findViewById(R.id.list_yoga);
        list_pranayama      = (RecyclerView) findViewById(R.id.list_pranayama);
        tv_last_steps       = (TextView) findViewById(R.id.tv_last_steps);
        tv_heading          = (TextView) findViewById(R.id.tv_heading);
        tv_yoga             = (TextView) findViewById(R.id.tv_yogasana);
        tv_pranayama        = (TextView) findViewById(R.id.tv_pranayama);
        tv_step_count       = (TextView) findViewById(R.id.tv_step_counter);

        floatingMenu        = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        action_setting      = (FloatingActionButton) findViewById(R.id.action_a);
        action_report       = (FloatingActionButton) findViewById(R.id.action_b);
        action_play         = (FloatingActionButton) findViewById(R.id.play);

        sv_main             = (ScrollChangeListner) findViewById(R.id.sv_main);

        reportModel         = new ReportModel();
        yoga_list           = new ArrayList<ReportModel>();
        pranayama_list      = new ArrayList<ReportModel>();
        list_time           = new ArrayList<ReportModel>();
        time                = new ArrayList<String>();
        db                  = new DatabaseHandler(context);

        adapter_yoga = new Recycler_adapter(yoga_list, "Yoga",context);
        list_yoga.setAdapter(adapter_yoga);
        adapter_pranayama = new Recycler_adapter(pranayama_list, "Pranayama",context);
        list_pranayama.setAdapter(adapter_pranayama);

        setDataAdapter();

        if (GlobalClass.callSavedPreferences("installdate", context).equals("0")) {
            SimpleDateFormat dateFormatsep = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String current_date = dateFormatsep.format(new Date());
            GlobalClass.savePreferences("installdate", current_date, context);
            GlobalClass.savePreferences2("preparation", 3, context);
        }

        if (GlobalClass.callSavedPreferences2("alarm_set", 0, context) == 0)
            setAlarm();

        action_setting.setIcon(R.drawable.setting_icon);
        action_report.setIcon(R.drawable.report_icon);
        action_play.setIcon(R.drawable.play_icon_normal);

        action_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MainReportActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        sv_main.setOnScrollChangedListener(mOnScrollChangedListener);

        action_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SettingActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        action_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Step_Counter_Activity.class));
                GlobalClass.savePreferences2("habit_start", 1, context);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //startActivity(new Intent(context,CatReportActivity.class));
            }
        });


        card_steps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Step_Counter_Activity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    ScrollChangeListner.OnScrollChangedListener mOnScrollChangedListener = new ScrollChangeListner.OnScrollChangedListener() {
        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            if (oldt > t)
                floatingMenu.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            else
                floatingMenu.animate().translationY(floatingMenu.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
        }
    };

    public void setDataAdapter(){
        String duration = "0 Min";
        yoga                = new String[]{getResources().getString(R.string.Veer), getResources().getString(R.string.Setu)};
        pranayama           = new String[]{getResources().getString(R.string.AnulomVilom), getResources().getString(R.string.Kapal), getResources().getString(R.string.Bhramari), getResources().getString(R.string.Report)};
        alternate_yoga      = new String[]{getResources().getString(R.string.Warrior), getResources().getString(R.string.Bridge)};
        alternate_pranayama = new String[]{getResources().getString(R.string.Alternate_nostril), getResources().getString(R.string.Skull_purification), getResources().getString(R.string.Humming_Bee_breath), getResources().getString(R.string.Report)};
        img_yoga            = new Integer[]{R.drawable.yoga_dd,R.drawable.demo_pranayama};
        img_prana           = new Integer[]{R.drawable.demo_pranayama,R.drawable.demo_pranayama,R.drawable.demo_pranayama,R.drawable.yoga_dd};

        db = new DatabaseHandler(context);

        yoga_list.clear();
        pranayama_list.clear();
        for (int i = 0; i < yoga.length; i++) {
            reportModel = new ReportModel();
            reportModel.setYogasana_name(yoga[i]);
            reportModel.setSubTitle(alternate_yoga[i]);
            reportModel.setImageId(img_yoga[i]);
            reportModel.setWish_list(1);
            yoga_list.add(reportModel);
        }
        for (int i = 0; i < pranayama.length; i++) {
            list_time   = db.getDoTime(i+1);
            reportModel = list_time.get(0);
            if (i==3)
                duration="";
            else {
                if (list_time.isEmpty())
                    duration = "0 Min";
                else
                    duration = reportModel.getDuration();
            }
            reportModel = new ReportModel();
            reportModel.setYogasana_name(pranayama[i]);
            reportModel.setSubTitle(alternate_pranayama[i]);
            reportModel.setDuration(duration);
            reportModel.setImageId(img_prana[i]);

            if (i<=1)
                reportModel.setWish_list(1);
            else if(i==2)
                reportModel.setWish_list(0);
            else
                reportModel.setWish_list(2);

            pranayama_list.add(reportModel);
        }

        adapter_yoga.notifyDataSetChanged();
        adapter_pranayama.notifyDataSetChanged();

    }

    public void setAlarm() {
        int hr = 7;
        int min = 0;
        GlobalClass.savePreferences2("hr", hr, context);
        GlobalClass.savePreferences2("min", min, context);
        GlobalClass.savePreferences2("alarm_set", 1, context);

        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hr);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        long triggerMillis = calendar.getTimeInMillis();

        if (calendar.getTimeInMillis() < Calendar.getInstance()
                .getTimeInMillis()) {
            triggerMillis = calendar.getTimeInMillis() + 24 * 60 * 60 * 1000;

            System.out.println("Alarm will go off next day");
        }

        am.setRepeating(AlarmManager.RTC_WAKEUP, triggerMillis, AlarmManager.INTERVAL_DAY, pendingIntent);
        Log.e("set alarm costum", hr + " : " + min);
    }

    public void changeLang() {
        String str_lang = "";
        switch (GlobalClass.callSavedPreferences2("language", 0, context)) {
            case 0:
                str_lang = "en";
                break;
            case 1:
                str_lang = "hi";
                break;
            case 2:
                str_lang = "ru";
                break;
            case 3:
                str_lang = "fr";
                break;
            case 4:
                str_lang = "de";
                break;
            case 5:
                str_lang = "es";
                break;
            default:
                str_lang = "en";
                break;
        }
        Locale myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_heading.setText(R.string.Category);
        tv_yoga.setText(R.string.Yogasana);
        tv_pranayama.setText(R.string.Pranayama);
        tv_step_count.setText(R.string.Step_counter);

        db = new DatabaseHandler(context);
        last_steps = db.getSteps("0");
        if (!last_steps.isEmpty()) {
            reportModel = last_steps.get(last_steps.size() - 1);
            tv_last_steps.setText(getResources().getString(R.string.Last_steps) + ": " + String.valueOf(reportModel.getTotal_steps()));
        } else
            tv_last_steps.setText(getResources().getString(R.string.Last_steps) + ": " + String.valueOf(0));

        setDataAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        GlobalClass.savePreferences2("habit_start",0,context);
        changeLang();
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }
}
