package com.habit.homescreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.habit.R;
import com.habit.anulomvilom.Anulom_Activity;
import com.habit.bhramari.BhramariPranayama_Activity;
import com.habit.data.ReportModel;
import com.habit.kapalbhati.Kapalbhati_Activity;
import com.habit.report.CatReportActivity;
import com.habit.setubandhasana.Activity_SetuBandhasana;
import com.habit.virbhadrasana.Virabhadrasana_Activity;

import java.util.List;

/**
 * Created by Hitesh on 05-Jan-17.
 */

public class Recycler_adapter extends RecyclerView.Adapter<Recycler_adapter.MyViewHolder> {

    private List<ReportModel> reportModelList;
    private String type;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_alternate,tv_time;
        ImageView iv_image,iv_wish;
        LinearLayout ll_main;
        CardView cardView;


        public MyViewHolder(View view) {
            super(view);
            tv_name         = (TextView) view.findViewById(R.id.tv_name);
            tv_alternate    = (TextView) view.findViewById(R.id.tv_altername_name);
            tv_time         = (TextView) view.findViewById(R.id.tv_time);
            iv_image        = (ImageView) view.findViewById(R.id.iv_image);
            iv_wish         = (ImageView) view.findViewById(R.id.iv_imagewish);
            ll_main         = (LinearLayout) view.findViewById(R.id.ll_main);
            cardView        = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public Recycler_adapter(List<ReportModel> reportModelList,String type,Context context) {
        this.reportModelList = reportModelList;
        this.type=type;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_yoga_pranayama, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ReportModel reportModel = reportModelList.get(position);
        if (type.equals("Pranayama")){

            final float scale = context.getResources().getDisplayMetrics().density;
            int pixels_width  = (int) (200 * scale + 0.5f);
            int pixels_height = (int) (200 * scale + 0.5f);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(pixels_width, pixels_height);
            holder.ll_main.setLayoutParams(layoutParams);

        }
        else {
            final float scale = context.getResources().getDisplayMetrics().density;
            int pixels_width  = (int) (250 * scale + 0.5f);
            int pixels_height = (int) (200 * scale + 0.5f);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(pixels_width, pixels_height);
            holder.ll_main.setLayoutParams(layoutParams);
        }
        holder.tv_name.setText(reportModel.getYogasana_name());
        holder.tv_alternate.setText(reportModel.getSubTitle());
        holder.tv_time.setText(reportModel.getDuration());
        holder.iv_image.setImageResource(reportModel.getImageId());

        if (reportModel.getWish_list()==1)
            holder.iv_wish.setBackgroundResource(R.color.colorPrimaryDark);
        else if (reportModel.getWish_list()==0)
            holder.iv_wish.setBackgroundColor(Color.parseColor("#66000000"));
        else
            holder.iv_wish.setVisibility(View.GONE);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("Pranayama")){

                    switch (position) {
                        case 0:
                            v.getContext().startActivity(new Intent(v.getContext(), Anulom_Activity.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                        case 1:
                            v.getContext().startActivity(new Intent(v.getContext(), Kapalbhati_Activity.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                        case 2:
                            v.getContext().startActivity(new Intent(v.getContext(), BhramariPranayama_Activity.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                        case 3:
                            v.getContext().startActivity(new Intent(v.getContext(), CatReportActivity.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                    }
                }
                else {
                    switch (position) {
                        case 0:
                            v.getContext().startActivity(new Intent(v.getContext(), Virabhadrasana_Activity.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                        case 1:
                            v.getContext().startActivity(new Intent(v.getContext(), Activity_SetuBandhasana.class));
                            ((Activity) v.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            break;
                    }
                }
            }
        });

        holder.iv_wish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*final Bitmap bmap = ((BitmapDrawable)holder.iv_wish.getDrawable()).getBitmap();
                Drawable myDrawable = v.getContext().getResources().getDrawable(R.drawable.wishlist_filled);
                final Bitmap myLogo = ((BitmapDrawable) myDrawable).getBitmap();*/
                ColorDrawable viewColor = (ColorDrawable) holder.iv_wish.getBackground();
                int colorId             = viewColor.getColor();
                if(colorId== v.getContext().getResources().getColor(R.color.colorPrimaryDark)) {
                    holder.iv_wish.setBackgroundResource(R.color.Wishlist_Empty);
                    holder.iv_wish.setBackgroundColor(Color.parseColor("#66000000"));
                    //holder.iv_wish.setAlpha(100);
                }
                else
                    holder.iv_wish.setBackgroundResource(R.color.colorPrimaryDark);

            }
        });
    }

    @Override
    public int getItemCount() {
        return reportModelList.size();
    }

}
