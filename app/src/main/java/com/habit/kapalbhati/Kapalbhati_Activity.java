package com.habit.kapalbhati;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.habit.MyNumberPicker;
import com.habit.R;
import com.habit.Sounds;
import com.habit.anulomvilom.ActivityPranayamaInfo;
import com.habit.bhramari.BhramariPranayama_Activity;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;
import com.habit.report.CatReportActivity;
import com.habit.report.ReportActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class Kapalbhati_Activity extends AppCompatActivity {
    //Reference Variable
    Context con;
    DrawerLayout drawer;
    NavigationView nav_view;

    ImageView im_inhel, im_pause, im_backbutton,tv_stop,tv_start,im_data,iv_reset,iv_help;
    TextView tv_inhalechange, tv_inhaltimer,tv_roundsreminder,tv_timerhr,tv_timermin,tv_timersec,tv_timerhr1,tv_timermin1,tv_timersec1,tv_kapalbh_h,tv_rounds_h,tv_time_h,tv_totaltime_h,tv_action_h,tv_roundsrem_h;
    LinearLayout ll_countdown,ll_start,ll_stop,content_kapal;
    MyNumberPicker np_rounds,np_time;
    int valuechangekapal;
    //Values of Inhale,Hold and Exhale
    int e=1,i=1,r;
    //r1 round reminder tm time
    int r1=r,tm=0;
    //rr use for decrease rounds when complete one round and m is use for on pause
    int rr=0,m=0;
    int hr, min, sec, tempsec,tt;
    String time;
    int tempI,tempE = 3;
    int flag=0;
    //p is for preparation time and rm is use for mentain the rounds reminder value on pause click and pau cound the pause
    int p,rm=0,pau=0;
    int tempP=p;
    //ther is thread
    int ther;
    //roun is use for mentain the value of time and round when app is closed
    int roun;
    //when pause button clicked three time then it increase by 1
    int increase=0;
    Thread t,t1;
    int totaltime;

    FloatingActionButton fab;
    Sounds sounds;
    int habit_start=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kapalbhati);

        con                 = this;
        im_data             = (ImageView) findViewById(R.id.im_data);
        im_inhel            = (ImageView) findViewById(R.id.im_inhale);
        im_pause            = (ImageView) findViewById(R.id.im_pause);
        iv_help             = (ImageView) findViewById(R.id.iv_help);
        im_backbutton       = (ImageView) findViewById(R.id.im_backbutton);
        tv_timerhr          = (TextView) findViewById(R.id.tv_timerhr);
        tv_timermin         = (TextView) findViewById(R.id.tv_timermin);
        tv_timersec         = (TextView) findViewById(R.id.tv_timersec);
        tv_timerhr1         = (TextView) findViewById(R.id.tv_timerhr1);
        tv_timermin1        = (TextView) findViewById(R.id.tv_timermin1);
        tv_timersec1        = (TextView) findViewById(R.id.tv_timersec1);
        tv_inhalechange     = (TextView) findViewById(R.id.tv_inhalechange);
        tv_inhaltimer       = (TextView) findViewById(R.id.tv_inhaletimer);

        tv_kapalbh_h        = (TextView) findViewById(R.id.tv_kapalbh_h);
        tv_rounds_h         = (TextView) findViewById(R.id.tv_round_h);
        tv_time_h           = (TextView) findViewById(R.id.tv_time_h);
        tv_roundsrem_h      = (TextView) findViewById(R.id.tv_roundre_h);
        tv_action_h         = (TextView) findViewById(R.id.tv_action_h);
        tv_totaltime_h      = (TextView) findViewById(R.id.tv_totaltime_h);

        tv_start            = (ImageView) findViewById(R.id.tv_start);
        tv_stop             = (ImageView) findViewById(R.id.tv_stop);
        tv_roundsreminder   = (TextView) findViewById(R.id.tv_roundsreminder);
        ll_countdown        = (LinearLayout) findViewById(R.id.ll_countdown1);
        ll_start            = (LinearLayout) findViewById(R.id.ll_start);
        ll_stop             = (LinearLayout) findViewById(R.id.ll_stop);
        iv_reset            = (ImageView) findViewById(R.id.iv_reset);

        np_rounds           = (MyNumberPicker) findViewById(R.id.np_rounds);
        np_time             = (MyNumberPicker) findViewById(R.id.np_time);

        content_kapal       = (LinearLayout) findViewById(R.id.content_kapal);
        fab                 = (FloatingActionButton) findViewById(R.id.fab);
        sounds              = new Sounds(con);
        habit_start         = GlobalClass.callSavedPreferences2("habit_start",habit_start,con);
        p                   = GlobalClass.callSavedPreferences2("preparation", p, con);

        numberPick();
        tv_roundsreminder.setText(String.valueOf(r));

//Start Button Functionality
        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });

//Stop Button
        tv_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                t1.interrupt();
                restart();
                flag = 0;
            }
        });
//Pause Button
        im_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.interrupt();
                t1.interrupt();
                tv_stop.setVisibility(View.INVISIBLE);
                tv_start.setVisibility(View.VISIBLE);
                tv_roundsreminder.setText(String.valueOf(r1));
                flag = 1;
                pau = 1;
                rm = 1;
                increase = increase + 1;
            }
        });

//back button
        im_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//report
        im_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(con, ReportActivity.class);
                in.putExtra("type",2);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
//reset button
        iv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetValue();
            }
        });
//if fab is open then close it on everywhere touch on screen
        ll_countdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


//floating button
        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(con,ActivityPranayamaInfo.class);
                intent.putExtra("type",2);
                startActivity(intent);
            }
        });
    }

    public void play(){
        tv_start.setVisibility(View.GONE);
        tv_stop.setVisibility(View.VISIBLE);
        ll_stop.setVisibility(View.GONE);
        ll_start.setVisibility(View.VISIBLE);
        timer();
        if(rm==0)
            tv_roundsreminder.setText(String.valueOf(r));
        //m is use for on pause
        m=1;
        //there are two timer in this then some time we pause the timer then bot timer are not same then we increase the rounds on every three time pause
        if(increase==3){
            r1=r1+1;
            increase=0;
        }
        setDisabled();
        if(flag==0) {
            if(p==0)
                tempI = i;
            r1=r;
            // tempI = i;
            tt=tempsec;
        }
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(ther);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (r1 > 0) {
                                    if(tempP <= p && tempP >= 0){
                                        if (tempP < 0)
                                            tempI = i;
                                    }
                                    else if (tempI <= i && tempI > 0) {
                                        tv_inhalechange.setText(R.string.Inhale_Normally);
                                        im_inhel.setImageResource(R.drawable.kapal_postion2);
                                        tv_inhaltimer.setVisibility(View.INVISIBLE);

                                        tempI = tempI - 1;

                                        if (tempI == 0)
                                            tempE = e;
                                        rr = rr + 1;

                                    } else if (tempE <= e && tempE >= 0) {
                                        if (tempE == e)
                                            sounds.playSoundExhaelKapal();
                                        tv_inhalechange.setText(R.string.Exhale_force);
                                        im_inhel.setImageResource(R.drawable.kapal_postion);

                                        tempE = tempE - 1;
                                        if (tempE == 0) {
                                            tempI = i;
                                            rr = rr + 1;
                                        }
                                    }
                                    if(rr==2) {
                                        //decrease the rounds reminder value after complete one round
                                        r1 = r1 - 1;
                                        tv_roundsreminder.setText(String.valueOf(r1));
                                        rr=0;
                                    }

                                }// outer if close
                                else{
                                    //intrupt both timer when complete the time
                                    sounds.playSound3();
                                    t.interrupt();
                                    t1.interrupt();
                                    restart();
                                }
                            }  // run close

                        }); // Runnable close

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }
    //for set the time
    public void setStartTime() {
        tempsec= tm*60;
        hr = tempsec / 3600;
        int v = tempsec % 3600;
        min = v / 60;
        sec = v % 60;
        time = String.format("%02d : %02d : %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

    //Change the time
    public void setCountDownTime() {
        hr = tempsec / 3600;
        int v = tempsec % 3600;
        min = v / 60;
        sec = v % 60;
        time = String.format("%02d : %02d : %02d", hr, min, sec);
        tv_timerhr.setText(String.valueOf(hr));
        tv_timermin.setText(String.valueOf(min));
        tv_timersec.setText(String.valueOf(sec));
        tv_timerhr1.setText(String.valueOf(hr));
        tv_timermin1.setText(String.valueOf(min));
        tv_timersec1.setText(String.valueOf(sec));
    }

    //Reset the all contents when complete or stop
    public void restart() {
        im_inhel.setImageBitmap(null);
        tv_inhalechange.setText(null);
        tv_inhaltimer.setText("");
        tv_roundsreminder.setText("");
        tv_start.setVisibility(View.VISIBLE);
        tv_stop.setVisibility(View.INVISIBLE);
        im_backbutton.setVisibility(View.VISIBLE);
        im_pause.setVisibility(View.INVISIBLE);
        tv_roundsreminder.setText(String.valueOf(r));
        ll_start.setVisibility(View.GONE);
        ll_stop.setVisibility(View.VISIBLE);
        ll_countdown.setEnabled(true);
        tv_inhaltimer.setVisibility(View.VISIBLE);
        setEnabled();
        tempP=p;
        i=1;
        e=1;
        increase=0;
        r1=r;
        int doTime=tt-tempsec;
        m=0;

        totaltime = doTime+totaltime;

        setStartTime();
        if(doTime!=0) {
            //send the contend to database
            DatabaseHandler db = new DatabaseHandler(con);
            db.addPranayamaTimes(String.format("%2d",tm), r, 2, tt, doTime);
           // db.addContact(tm,0,0, r, 2, tt, doTime,str_date,str_datesep,"null");
        }
        m=0;
        if (habit_start==1){
            startActivity(new Intent(con, BhramariPranayama_Activity.class));
        }
    }

    //Method for change the colour and disable the arrow of inhale and rounds
    public void setDisabled() {
        im_data.setVisibility(View.INVISIBLE);
        ll_countdown.setEnabled(false);
        im_backbutton.setVisibility(View.INVISIBLE);
        iv_reset.setVisibility(View.INVISIBLE);
        im_pause.setVisibility(View.VISIBLE);
        iv_help.setVisibility(View.GONE);
    }

//Method for change the colour and enable the arrow of inhale and rounds

    public void setEnabled() {
        im_pause.setVisibility(View.GONE);
        iv_help.setVisibility(View.VISIBLE);
        iv_reset.setVisibility(View.VISIBLE);
        ll_countdown.setEnabled(true);
        im_backbutton.setVisibility(View.VISIBLE);
        im_data.setVisibility(View.VISIBLE);
    }

    public void changeLang() {
        String str_lang = "";
        switch (GlobalClass.callSavedPreferences2("language", 0, con)){
            case 0:
                str_lang="en";
                break;
            case 1:
                str_lang="hi";
                break;
            case 2:
                str_lang="ru";
                break;
            case 3:
                str_lang="fr";
                break;
            case 4:
                str_lang="de";
                break;
            case 5:
                str_lang="es";
                break;
            default:
                str_lang="en";
                break;
        }
        Locale myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_kapalbh_h.setText(R.string.Kapal);
        tv_rounds_h.setText(R.string.Rounds);
        tv_time_h.setText(R.string.Time);
        tv_roundsrem_h.setText(R.string.Rounds);
        tv_action_h.setText(R.string.Action);
        tv_totaltime_h.setText(R.string.Total_Time);
    }

    //main timer that start on start
    public void timer(){
        if(flag==0) {
            tempP = p;
        }
        t1 = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCountDownTime();
                                if (tempsec > 0) {
                                    if(tempP <=p && tempP >= 0){
                                        if(p!=0 && tempP>0){
                                            sounds.playSound1();
                                            tv_inhalechange.setText(R.string.Prepare);
                                            tv_inhaltimer.setText(String.valueOf(tempP));
                                            im_inhel.setImageBitmap(null);

                                        }
                                        tempP = tempP - 1;
                                        if (tempP <= 0)
                                            tempI= i;
                                    }
                                    if(tempP<0)
                                        tempsec = tempsec - 1;
                                }
                                else
                                    t1.interrupt();
                            }
                            // run close

                        }); // Runnable close

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t1.start();
    }

    //when click on reset then reset the value of time and rounds
    public void resetValue(){
        r = 60;
        tm=1;
        roun=r;
        ther=(30000/r)*tm;
        setStartTime();
        np_rounds.setMinValue(tm*10);
        np_rounds.setMaxValue(tm*150);
        np_rounds.setValue(r);
        np_time.setValue(tm);
        GlobalClass.savePreferences2("tmkapal", tm, con);
        GlobalClass.savePreferences2("roundkapal", r, con);
    }

    public void numberPick(){
//make divider transparent
        setDividerColor(np_time, Color.TRANSPARENT);
        setDividerColor(np_rounds, Color.TRANSPARENT);
        valuechangekapal=GlobalClass.callSavedPreferences2("valuechangekapal",valuechangekapal,con);

        if(valuechangekapal==2) {
            r = GlobalClass.callSavedPreferences2("roundkapal", r, con);
            tm = GlobalClass.callSavedPreferences2("tmkapal", tm, con);
        }
        else{
            r=60;
            tm=1;
            GlobalClass.savePreferences2("tmkapal", tm, con);
            GlobalClass.savePreferences2("roundkapal", r, con);
        }
//set minimum and maximum values

        np_time.setMinValue(1);
        np_time.setMaxValue(20);
        np_time.setValue(tm);

        np_rounds.setMinValue(tm*10);
        np_rounds.setMaxValue(tm*150);
        np_rounds.setValue(r);
        ther=(30000/r)*tm;
        roun=r;
        setStartTime();

        np_rounds.setWrapSelectorWheel(true);
        np_rounds.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                r=newVal;
                tm=np_time.getValue();
                roun=r/tm;
                ther=(30000/r)*tm;
                GlobalClass.savePreferences2("tmkapal", tm, con);
                GlobalClass.savePreferences2("roundkapal", r, con);
                GlobalClass.savePreferences2("valuechangekapal",2,con);
            }
        });

        np_time.setWrapSelectorWheel(true);
        np_time.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                r=np_rounds.getValue();
                roun=r/oldVal;
                tm=newVal;
                r=roun*tm;
                ther=(30000/r)*tm;
                np_rounds.setMinValue(tm*10);
                np_rounds.setMaxValue(tm*150);
                np_rounds.setValue(r);

                GlobalClass.savePreferences2("tmkapal", tm, con);
                GlobalClass.savePreferences2("roundkapal", r, con);
                GlobalClass.savePreferences2("valuechangekapal",2,con);
                setStartTime();
            }
        });
    }

    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        sounds     = new Sounds(con);
        changeLang();
        if (GlobalClass.callSavedPreferences1("display", con))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onPause() {
        super.onPause();
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if(m==1)
            t.interrupt();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        GlobalClass.savePreferences2("habit_start",0,con);
    }
}