package com.habit.report;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.habit.R;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.data.ReportModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;


public class CatReportActivity extends ActionBarActivity {

    ListView listview;
    Context mContext;
    ImageView im_delete,iv_back;
    TextView tv_report,tv_no,tv_date,tv_duration,tv_type;

    DatabaseHandler dbHandler;

    ArrayList<ReportModel> mlist;

    ReportAdapter_Cat adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_reportcat);

        mContext    = this;
        tv_report   =(TextView)findViewById(R.id.tv_report_h);
        tv_no       =(TextView)findViewById(R.id.tv_no);
        tv_date     =(TextView)findViewById(R.id.tv_date);
        tv_duration =(TextView)findViewById(R.id.tv_duration);
        tv_type     =(TextView)findViewById(R.id.tv_type);
        iv_back     =(ImageView)findViewById(R.id.im_backbutton);
        listview    =(ListView) findViewById(R.id.listView);

        changeLang();

        dbHandler=new DatabaseHandler(mContext);
        mlist=dbHandler.getAllData();
        Collections.reverse(mlist);

        if(mlist.isEmpty())
        {
            TextView tv=(TextView)findViewById(R.id.lv_textview);
            tv.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        } else {

            // Pass results to ListViewAdapter Class
            adapter = new ReportAdapter_Cat(mContext, R.layout.adapter_cat_report,
                    mlist);

            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);
            listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            // Capture ListView item click
            listview.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

                @Override
                public void onItemCheckedStateChanged(ActionMode mode,
                                                      int position, long id, boolean checked) {
                    // Capture total checked items
                    final int checkedCount = listview.getCheckedItemCount();
                    // Set the CAB title according to total checked items
                    mode.setTitle(checkedCount + " Selected");
                    // Calls toggleSelection method from ListViewAdapter Class
                    adapter.toggleSelection(position);
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.delete:
                            // Calls getSelectedIds method from ListViewAdapter Class
                            SparseBooleanArray selected = adapter
                                    .getSelectedIds();
                            // Captures all selected ids with a loop
                            for (int i = (selected.size() - 1); i >= 0; i--) {
                                if (selected.valueAt(i)) {
                                    ReportModel selecteditem = adapter
                                            .getItem(selected.keyAt(i));
                                    // Remove selected items following the ids
                                    adapter.remove(selecteditem);
                                    dbHandler.deleteAll(selecteditem.getDate(),selecteditem.getType1());
                                    if(mlist.isEmpty()){
                                        TextView tv=(TextView)findViewById(R.id.lv_textview);
                                        tv.setVisibility(View.VISIBLE);
                                        listview.setVisibility(View.GONE);
                                    }
                                }
                            }
                            // Close CAB
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    mode.getMenuInflater().inflate(R.menu.activity_main, menu);
                    return true;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    // TODO Auto-generated method stub
                    adapter.removeSelection();
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    // TODO Auto-generated method stub
                    return false;
                }
            });

        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void changeLang() {
        String str_lang = "";
        switch (GlobalClass.callSavedPreferences2("language", 0, mContext)){
            case 0:
                str_lang="en";
                break;
            case 1:
                str_lang="hi";
                break;
            case 2:
                str_lang="ru";
                break;
            case 3:
                str_lang="fr";
                break;
            case 4:
                str_lang="de";
                break;
            case 5:
                str_lang="es";
                break;
            default:
                str_lang="en";
                break;
        }
        Locale myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts(){
        tv_report.setText(R.string.Report);
        tv_date.setText(R.string.Date);
        tv_duration.setText(R.string.Duration);
        tv_type.setText(R.string.Type);
    }


    @Override
    public void onResume(){
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", mContext))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
