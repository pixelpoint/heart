package com.habit.report;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.habit.R;
import com.habit.data.ReportModel;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter_Cat extends ArrayAdapter<ReportModel> {

    Context con;
    ArrayList<ReportModel> mlist;
    ReportModel reportModel;
    SparseBooleanArray mSelectedItemsIds;


    public ReportAdapter_Cat(Context con, int resourceId, ArrayList<ReportModel> mlist)
    {
        super(con, resourceId, mlist);
        this.con=con;
        this.mlist=mlist;
        mSelectedItemsIds = new SparseBooleanArray();

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder=null;
        reportModel =mlist.get(position);

        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView==null){


            convertView=inflater.inflate(R.layout.adapter_cat_report,parent,false);
            holder=new ViewHolder();

      //      holder.textid=(TextView)convertView.findViewById(R.id.tv_No);
            holder.textdate=(TextView)convertView.findViewById(R.id.tv_Date);
            holder.textduration=(TextView)convertView.findViewById(R.id.tv_Duration);
            holder.texttype=(TextView)convertView.findViewById(R.id.tv_Type);

            convertView.setTag(holder);

        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }

  //      holder.textid.setText(reportModel.getId());
        holder.textdate.setText(reportModel.getDate());
        holder.textduration.setText(reportModel.getDuration());
        switch (reportModel.getType1()){

            case 1:
                holder.texttype.setText(R.string.AnulomVilom);
                break;
            case 2:
                holder.texttype.setText(R.string.Kapal);
                break;
            case 3:
                holder.texttype.setText(R.string.Bhramari);
                break;
        }

        return convertView;
    }

    private class ViewHolder{

        TextView textdate;
        TextView textduration;
        TextView texttype;

    }

    @Override
    public void remove(ReportModel object) {
        mlist.remove(object);
        notifyDataSetChanged();
    }

    public List<ReportModel> getWorldPopulation() {
        return mlist;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
