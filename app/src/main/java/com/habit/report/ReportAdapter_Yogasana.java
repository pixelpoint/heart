package com.habit.report;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.habit.R;
import com.habit.data.ReportModel;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter_Yogasana extends ArrayAdapter<Integer> {

    Context con;
    ArrayList<Integer> mlist;
    ReportModel reportModel;
    SparseBooleanArray mSelectedItemsIds;


    public ReportAdapter_Yogasana(Context con, int resourceId, ArrayList<Integer> mlist)
    {
        super(con, resourceId, mlist);
        this.con=con;
        this.mlist=mlist;
        mSelectedItemsIds = new SparseBooleanArray();

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder=null;
        //reportModel =mlist.get(position);

        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView==null){


            convertView=inflater.inflate(R.layout.adapter_yogasana_report,parent,false);
            holder=new ViewHolder();

            holder.iv_thumb = (ImageView) convertView.findViewById(R.id.tv_name);

            convertView.setTag(holder);

        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }

       // holder.textname.setText("");
        //String status=reportModel.getCustom_habit_status();

        if (mlist.get(position)==1) {
           // holder.iv_thumb.setImageResource(R.drawable.thumbs_up);

            //holder.iv_thumb.getResources().setColorFilter(new PorterDuffColorFilter(Color.parseColor("#c1185b"), PorterDuff.Mode.MULTIPLY));
        }
        else {
          //  holder.iv_thumb.setImageResource(R.drawable.thumbs_down);
            //holder.iv_thumb.setRotation(180);
            holder.iv_thumb.setRotationX(180);
           // holder.textname.getBackground().setColorFilter(new PorterDuffColorFilter(Color.parseColor("#c1185b"), PorterDuff.Mode.MULTIPLY));
        }

        return convertView;
    }

    private class ViewHolder{

        ImageView iv_thumb;


    }

    /*@Override
    public void remove(ReportModel object) {
        mlist.remove(object);
        notifyDataSetChanged();
    }*/

    public List<Integer> getWorldPopulation() {
        return mlist;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
