package com.habit.setubandhasana;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.habit.R;
import com.habit.anulomvilom.Anulom_Activity;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;

import java.util.Locale;

public class Activity_SetuBandhasana extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    Context context;
    Boolean display;
    Locale myLocale;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    FloatingActionButton fab;
    int notify;
    ImageView imbackbutton;
    TextView tv_setu;
    int habit_start=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_setu_bandhasana);

        context         =this;
        imbackbutton    =(ImageView) findViewById(R.id.im_backbutton);
        fab             = (FloatingActionButton) findViewById(R.id.fab);
        viewPager       = (ViewPager) findViewById(R.id.pager_setu);
        tabLayout       = (TabLayout) findViewById(R.id.tab_setu);
        tv_setu         = (TextView) findViewById(R.id.tv_anulom);
        habit_start     = GlobalClass.callSavedPreferences2("habit_start",habit_start,context);

        //changeLang();

        tabLayout.addTab(tabLayout.newTab().setText(R.string.Steps));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Benefits));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Precautions));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Pager_SetuBandhasana adapter = new Pager_SetuBandhasana(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(this);

        imbackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (habit_start==1)
            fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });
    }
    public void dialog(){
        final Intent anulom = new Intent(context,Anulom_Activity.class);

        new AlertDialog.Builder(this).setTitle("Task").setMessage("Do you have completed Setubandhasana task for the day?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(anulom);
                DatabaseHandler db=new DatabaseHandler(context);
                db.addYoga(2,0,1);
                dialog.dismiss();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(anulom);
                DatabaseHandler db=new DatabaseHandler(context);
                db.addYoga(2,0,0);
                dialog.dismiss();
            }
        }).show();
    }

    /*public void changeLang() {
        String str_lang = "";
        if (lang_chng == 1) {
            str_lang = "hi";
        } else if(lang_chng == 2) {
            str_lang = "ru";
        } else if(lang_chng == 3) {
            str_lang = "fr";
        }
        else if (lang_chng == 4)
            str_lang = "de";
        else if (lang_chng == 5)
            str_lang = "es";
        else
            str_lang = "en";
        myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_setu.setText(R.string.Setu);
    }*/

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

 /*   public void dialog(){
        final Intent anulom  = new Intent(context,Anulom_Activity.class);
        new AlertDialog.Builder(this,R.style.MyDialogTheme).setTitle(R.string.Task).setMessage(R.string.Setu_task).setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GlobalClass.savePreferences("Setu","Yes",context);
                GlobalClass.savePreferences2("inten",2,context);
                startActivity(anulom);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.Nahi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GlobalClass.savePreferences("Setu","No",context);
                GlobalClass.savePreferences2("inten",2,context);
                startActivity(anulom);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                dialog.dismiss();
            }
        }).show();
    }*/

    @Override
    public void onResume(){
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        GlobalClass.savePreferences2("habit_start",0,context);

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

}
