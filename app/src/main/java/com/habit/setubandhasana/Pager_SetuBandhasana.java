package com.habit.setubandhasana;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by india on 27-Jul-16.
 */
public class Pager_SetuBandhasana extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager_SetuBandhasana(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Steps_SetuBandhasana steps= new Steps_SetuBandhasana();
                return steps;
            case 1:
                Benefits_SetuBandhasana benefits = new Benefits_SetuBandhasana();
                return benefits;
            case 2:
                Precautions_SetuBandhasana precautions = new Precautions_SetuBandhasana();
                return precautions;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
