package com.habit.virbhadrasana;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by india on 28-Jul-16.
 */
public class Pager_Virabhadrasana extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager_Virabhadrasana(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Steps_Virabhadrasana steps= new Steps_Virabhadrasana();
                return steps;
            case 1:
                Benefits_Virabhadrasana benefits = new Benefits_Virabhadrasana();
                return benefits;
            case 2:
                Precautions_Virabhadrasana precautions = new Precautions_Virabhadrasana();
                return precautions;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
