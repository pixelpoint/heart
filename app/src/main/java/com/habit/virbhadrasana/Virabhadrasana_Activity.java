package com.habit.virbhadrasana;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.habit.R;
import com.habit.data.DatabaseHandler;
import com.habit.data.GlobalClass;
import com.habit.setubandhasana.Activity_SetuBandhasana;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Virabhadrasana_Activity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    Context context;
    Boolean display;
    int lang_chng;
    Locale myLocale;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView imbackbutton;
    FloatingActionButton fab;
    TextView tv_virabhadrasana;
    int habit_start=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_virabhadrasana_);

        context             = this;
        fab                 = (FloatingActionButton) findViewById(R.id.fab);
        tabLayout           = (TabLayout) findViewById(R.id.tab_virabhadrasana);
        viewPager           = (ViewPager) findViewById(R.id.pager_virabhadrasana);
        tv_virabhadrasana   = (TextView) findViewById(R.id.tv_anulom);
        imbackbutton        = (ImageView) findViewById(R.id.im_backbutton);
        habit_start         = GlobalClass.callSavedPreferences2("habit_start",habit_start,context);

        //changeLang();

        tabLayout.addTab(tabLayout.newTab().setText(R.string.Steps));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Benefits));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Precautions));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.setCurrentItem(1, true);

        Pager_Virabhadrasana adapter = new Pager_Virabhadrasana(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(this);

        imbackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (habit_start==1)
            fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });
    }
    public void changeLang() {
        lang_chng = GlobalClass.callSavedPreferences2("spinnerSelection", lang_chng, context);
        String str_lang = "";
        if (lang_chng == 1) {
            str_lang = "hi";
        } else if(lang_chng == 2) {
            str_lang = "ru";
        } else if(lang_chng == 3) {
            str_lang = "fr";
        }
        else if (lang_chng == 4)
            str_lang = "de";
        else if (lang_chng == 5)
            str_lang = "es";
        else
            str_lang = "en";
        myLocale = new Locale(str_lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void updateTexts() {
        tv_virabhadrasana.setText(R.string.Veer);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void dialog(){
        final Intent balasana = new Intent(context,Activity_SetuBandhasana.class);

        new AlertDialog.Builder(this).setTitle("Task").setMessage("Do you have completed Virbhadrasana task for the day?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(balasana);
                DatabaseHandler db=new DatabaseHandler(context);
                db.addYoga(1,0,1);
                dialog.dismiss();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(balasana);
                DatabaseHandler db=new DatabaseHandler(context);
                db.addYoga(1,0,0);
                dialog.dismiss();
            }
        }).show();
    }
    /*public void dialog_Custom_habit(){

        new AlertDialog.Builder(this,R.style.MyDialogTheme).setTitle(R.string.Task).setMessage(R.string.Vipritkarani_task).setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseHandler dbHandler=new DatabaseHandler(context);
                AutoIntent_Notification autoIntent_notification =new AutoIntent_Notification(context);
                int habit_id= GlobalClass.callSavedPreferences2("habit_id_alarm",0,context);
                dbHandler.update_CustomProgress(habit_id,"Yes",autoIntent_notification.index_name(),autoIntent_notification.currentDate());
                autoIntent_notification.start_intent();
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.Nahi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AutoIntent_Notification autoIntent_notification=new AutoIntent_Notification(context);
                autoIntent_notification.start_intent();
                dialog.dismiss();
            }
        }).show();
    }*/


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onResume(){
        super.onResume();
        if (GlobalClass.callSavedPreferences1("display", context))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        GlobalClass.savePreferences2("habit_start",0,context);
    }
}
